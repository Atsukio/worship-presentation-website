<?php require_once 'include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="<?php echo $const['LOCATION']; ?>css/timeline.css">
<link rel="stylesheet" type="text/css" href="<?php echo $const['LOCATION']; ?>css/profilecard.css">
</head>
<body class="w3-animate-opacity">
<?php require_once 'include/header.php'; ?>
<!-- Timeline -->
<div class="w3-container w3-center w3-content" style="max-height:33vmax; overflow-y:auto;">
  <div class="w3-center w3-container w3-white">
	<h2 style="font-size:4vmin;">Current Timeline / Progress</h2>
  </div>
  <div class="timeline">
    <div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
        <h2>Early October</h2>
        <p> - Profile pages can now have profile pictures (avatars)</p>
        <p> - Profile page will now show an error if username is invalid or
        if user is trying to view their current profile without being logged in</p>
        <p> - Added roles management page</p>
        <p> - Added registration page</p>
        <p> - User's avatar is now shown next to the username</p>
      </div>
    </div>
    <div class="timeline-container timeline-right">
      <div class="timeline-content">
        <h2>2018</h2>
	      <h2>Late September</h2>
        <p> - Navigation now uses a hyrid of top navigation and sidebar</p>
        <p> - Worship controls are now fully functional, with cookies to update status</p>
        <p> - Added profiles for users</p>
        <p> - Made urls cleaner by htaccess</p>
        <p> - Signout now uses AJAX</p>
        <p> - Login form now a lot more user friendly, by having enter key login and clicking
           on the login button automatically focuses on the username input
        </p>
      </div>
    </div>
    <div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
        <h2>Mid September</h2>
        <p> - Changed home edit button color</p>
        <p> - Added carousel image editing button placeholder</p>
        <p> - Song list is now mobile friendly, looks a lot cleaner</p>
        <p> - Song list now becomes fixed with medium and small screens, but abosulute on large</p>
        <p> - Worship playlist and song list resizes on screen size change</p>
        <p> - Navigation gets hidden during mobile song list view but resets on large screen change</p>
        <p> - Modified Worship status code</p>
      </div>
    </div>
    <div class="timeline-container timeline-right">
      <div class="timeline-content">
        <h2>2018</h2>
	      <h2>Early September</h2>
        <p> - Bug fixes on music playlist after deleting doesn't move sequences</p>
        <p> - Moved duplicate codes for Music and Worship page to a single file</p>
        <p> - Music editor added default text for selection</p>
        <p> - Moved misaligned Verses and Sequence box in Music Page</p>
        <p> - Removed search in worship page</p>
        <p> - Editing home page title description, Music page editor, playlist,
        Worship page broadcast controls now require login permissions</p>
      </div>
    </div>
    <div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
        <h2>Late August</h2>
        <p> - Home page texts can now be edited</p>
        <p> - Retrieving the song list is now immensely faster by using a MYSQLI statement over javascript sort</p>
        <p> - Website has been validated by w3c validator</p>
        <p> - W.I.P. Broadcasting controls and broadcaster</p>
        <p> - Currently shows ON-AIR if someone is taking control and OFF-AIR if no one is</p>
        <p> - Broadcast controls now working (START / OVERIDE / END)</p>
        <p> - MYSQLI statements are now using Prepared statements to prevent SQL Injections</p>
        <p> - Music response statements are now using this.responseText instead of pre-typed ones, which allows error messages</p>
        <p> - Added follow broadcaster button that hides if you're the broadcaster</p>
      </div>
    </div>
    <div class="timeline-container timeline-right">
      <div class="timeline-content">
        <h2>2018</h2>
	      <h2>Mid August</h2>
        <p> - Moved major variables to objects</p>
        <p> - Rehaul of the Music.php page semi complete</p>
        <p> - Songs can now be tranposed (Minor and Major Keys only)</p>
		    <p> - Scroll Mode now fully functioning</p>
        <p> - Music sheet can now be printed</p>
        <p> - Songs on the playlist tab can now be selected</p>
        <p> - Index.php slideshow is now easier to add or remove images</p>
      </div>
    </div>
    <div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
	    <h2>Early August</h2>
        <p> - Fullscreen button can now toggle fullscreen mode</p>
  		  <p> - Broken worship page, will fix in the future</p>
  		  <p> - Well i messed up, complete rehaul of music.php has to be done</p>
      </div>
    </div>
    <div class="timeline-container timeline-right">
      <div class="timeline-content">
        <h2>2018</h2>
	      <h2>Late July</h2>
        <p> - Most likely no progress as final exams are coming up</p>
		    <p> - Final exams in progress</p>
      </div>
    </div>
    <div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
	      <h2>Mid July</h2>
        <p> - Had a meeting, color changed to light green for navigation and light gray music sheet background</p>
      </div>
    </div>
    <div class="timeline-container timeline-right">
      <div class="timeline-content">
        <h2>2018</h2>
	    <h2>Early July</h2>
        <p> - No progress was made due to assignments</p>
      </div>
    </div>
	<div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
	    <h2>Late June</h2>
        <p> - Functioning worship playlist</p>
      </div>
    </div>
    <div class="timeline-container timeline-right">
      <div class="timeline-content">
        <h2>2018</h2>
	    <h2>Mid June</h2>
        <p> - Developed finish the music editor</p>
		<p> - Developed finish the music sheet viewer</p>
      </div>
    </div>
    <div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
	    <h2>Early June</h2>
        <p> - Samuel finally read finish HTML and PHP</p>
		<p> - Samuel finally starts working the website</p>
      </div>
    </div>
    <div class="timeline-container timeline-right">
      <div class="timeline-content">
        <h2>2018</h2>
  	    <h2>Late May</h2>
        <p>- Developed the website template</p>
	    <p>- Designed the Music Editor template</p>
      </div>
    </div>
    <div class="timeline-container timeline-left">
      <div class="timeline-content">
        <h2>2018</h2>
	    <h2>Early May</h2>
        <p>Interviewed Transfornation for concepts</p>
      </div>
    </div>
  </div>
</div>

<!-- Team Members -->
<div class="w3-light-grey w3-row-padding">
  <div class="w3-half w3-section">
    <div class="profile-card">
	  <div class="w3-display-container">
      <img src="<?php echo $const['LOCATION']; ?>images/img_avatar.png" alt="Samuel" style="width:100%">
  		<div class="w3-padding w3-display-middle">
        <i style="font-size:5em;color:pink;" class="fas fa-american-sign-language-interpreting"></i>
      </div>
	  </div>
      <h1>Samuel Yong Syn Wei</h1>
      <p class="profile-card-title">Co-Developer</p>
      <p>International College of Advanced Technology Sarawak</p>
      <a href="#"><i class="fab fa-dribbble"></i></a>
      <a href="#"><i class="fab fa-twitter"></i></a>
      <a href="#"><i class="fab fa-linkedin"></i></a>
      <a href="#"><i class="fab fa-facebook"></i></a>
      <p><button>Contact</button></p>
    </div>
  </div>
  <div class="w3-half w3-section">
    <div class="profile-card">
      <div class="w3-display-container">
        <img src="<?php echo $const['LOCATION']; ?>images/img_avatar.png" alt="Jonathan" style="width:100%">
    		<div class="w3-padding w3-display-middle">
          <i style="font-size:6em;color:black;padding-bottom:36px;" class="far fa-smile"></i>
        </div>
  	  </div>
      <h1>Jonathan Chung Ka Heng</h1>
      <p class="profile-card-title">Co-Developer</p>
      <p>International College of Advanced Technology Sarawak</p>
      <a href="#"><i class="fab fa-dribbble"></i></a>
      <a href="#"><i class="fab fa-twitter"></i></a>
      <a href="#"><i class="fab fa-linkedin"></i></a>
      <a href="#"><i class="fab fa-facebook"></i></a>
      <p><button>Contact</button></p>
    </div>
  </div>
</div>
<?php require_once 'include/footer.php'; ?>
</body>
</html>
