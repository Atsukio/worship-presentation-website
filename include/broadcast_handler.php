<?php
require_once 'mysqli_connect.php';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
  if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_BROADCAST,$validRoles)) {
    if(isset($_POST['input']) && !empty($_POST['input'])) {
      switch($_POST['input']) {

        // Start broadcast
        case "start":
        date_default_timezone_set('Asia/Singapore');
        $sql = 'INSERT INTO ' . TB_LIVE .
        ' (current_song, current_sequence, userID, date_modified) ' .
        'VALUES (?, ?, ?, ?)';
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("iiis", $song, $seq, $validId, $date);
        $date = date("Y-m-d G:i:s");
        if(isset($_POST['song']) && !empty($_POST['song']) && $_POST['song'] != -1 && is_numeric($_POST['song'])) {
          $song = $_POST['song'];
        } else {
          $sql = 'SELECT song_id FROM ' . TB_PLAYLIST . ' WHERE seq_no=0';
          if(!$result = $mysqli->query($sql)) {
            die("Error: Failed to retrieve song id");
          }
          $row = $result->fetch_assoc();
          $song = $row['song_id'];
        }
        if(isset($_POST['seq']) && !empty($_POST['seq']) && $_POST['seq'] != -1 && is_numeric($_POST['seq'])) {
          $seq = $_POST['seq'];
        } else {
          $seq = 0;
        }
        $row = $mysqli->query("SELECT COUNT(id) AS count FROM " . TB_LIVE)->fetch_assoc();
        if($row['count'] == 0) {
          if(!$ps->execute()) {
            die("Error: Couldn't start broadcast");
          }
          die('success');
        }
        break;

        // End broadcast
        case "end":
        $sql = 'SELECT count(userID) AS count
        FROM ' . TB_LIVE .
        ' WHERE userID = ?';
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("i", $row['id']);
        if(!$ps->execute()) {
          die("Error: Something went wrong while searching for your account");
        }
        $row = $ps->get_result()->fetch_assoc();
        if($row['count'] == 0) {
          die("Error: Invalid account, please try relogging in");
        } else {
          $sql = 'TRUNCATE TABLE ' . TB_LIVE;
          if($mysqli->query($sql)) {
            die('success');
          } else {
            die("Error: Couldn't stop the broadcast");
          }
        }
        break;

        // Override broadcast
        case "override":
        $sql = 'UPDATE ' . TB_LIVE .
        ' SET userID = ?';
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("i", $validId);
        if(!$ps->execute()) {
          die("Error: Something went wrong, please try refreshing the page");
        } else {
          die('success');
        }
        break;


        // Change broadcast song
        case "song":
        $sql = 'UPDATE ' . TB_LIVE .
        ' SET current_song = ?';
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("i", $_POST['id']);
        if(!$ps->execute()) {
          die("Error: Something went wrong, please try refreshing the page");
        }
        $sql = 'UPDATE ' . TB_LIVE .
        ' SET current_sequence = 0';
        if(!$mysqli->query($sql)) {
          die("Error: Failed to reset sequence of song, please change sequence to update");
        }
        break;

        // Change broadcast sequence
        case "sequence":
        $sql = 'UPDATE ' . TB_LIVE .
        ' SET current_sequence = ?';
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("i", $_POST['seq']);
        if(!$ps->execute()) {
          die("Error: Something went wrong, please try refreshing the page");
        }
        break;

        default:
        die('Error: Something went wrong here, please refresh the page');
      }
    }
  }
}
?>
