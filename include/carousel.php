<?php
// Check for valid roles
if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
echo <<<HEREDOC
<script>
// Display carousel images and display them into the editor's gallery
function galleryImgs() {
	var xmlhttp = new XMLHttpRequest;
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (this.responseText == "Error") {
				$("#uploadStatus").html("Failed to retrieve images");
			} else {
				$("#crslGalleryDiv").html(this.responseText);
			}
		}
	};
	xmlhttp.open("POST", "{$const['LOCATION']}include/index_handler.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("type=gallery");
}

// Save current selection of images into database
function saveImgs() {
	var xmlhttp = new XMLHttpRequest;
	var datastring = "id=";
	var x = $( "#crslForm" ).serializeArray();
	let roles = false;
	$.each(x, function(i, field){
		if(field.name == "crslImgs") {
			if(roles) {
				datastring += "," + field.value;
			} else {
				if(datastring) {
					datastring += field.value;
					roles = true;
				}
			}
		}
	});
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (this.responseText == "Error") {
				$("#uploadStatus").html("Failed to save changes");
			} else {
				$("#uploadStatus").html("<b class='w3-text-green'>Changes saved successfully, refresh to take effect.</b>");
			}
		}
	};
	xmlhttp.open("POST", "{$const['LOCATION']}include/index_handler.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("type=save&" + datastring);
}

// Delete image from database and file
function deleteImg(value) {
	var xmlhttp = new XMLHttpRequest;
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (this.responseText == "Error") {
				$("#uploadStatus").html("Failed to delete song");
			} else {
				$("#uploadStatus").html("<b class='w3-text-green'>Song has been deleted</b>");
				galleryImgs();
			}
		}
	};
	xmlhttp.open("POST", "{$const['LOCATION']}include/index_handler.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("type=delete&img=" + value);
}

// When document is loaded finished
$(document).ready(function(e){

	var crslEditor = document.getElementById('crslEditor');

	window.onclick = function(event) {
	 if (event.target == crslEditor) {
		 document.body.style.overflow = "auto";
		 crslEditor.style.display = "none";
	 }
	}

	// AJAX form
	$("#crslForm").on('submit',(function(e) {
   e.preventDefault();
   $.ajax({
    url: "{$const['LOCATION']}include/index_handler.php",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
          cache: false,
    processData:false,
    beforeSend : function() {
     $("#uploadStatus").html("<b class='w3-text-blue'>Uploading</b>").fadeIn();
    },
    success: function(data) {
     if(data=='invalid') {
      // invalid file format.
      $("#uploadStatus").html("Invalid File !").fadeIn();
     } else {
			 $("#uploadStatus").html(data).fadeIn();
       // view uploaded file.
       galleryImgs();
     }
   },
   error: function(e) {
     $("#uploadStatus").html(e).fadeIn();
   }
   });
 }));
});
</script>
HEREDOC;
}
?>
<!-- Slide Show -->
<div class="w3-display-container w3-card w3-image w3-col" style="height:80vmin;overflow:hidden;">
  <?php
	$result = $mysqli->query("SELECT img_location FROM {$const['TB_CRSL']} WHERE visible = 1");
	while($row = $result->fetch_assoc()) {
		echo(
			'<img class="Slides w3-animate-right" src="' . $const['CAROUSEL'] . $row['img_location'] .
      '" style="width:100%; height:100%;" alt="' . $row['img_location'] . '">'
		);
	}
  if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
    echo <<<HEREDOC
    <div>
      <div class="w3-display-middle w3-display-hover">
        <button class="w3-btn w3-light-gray w3-hover-gray w3-card w3-round-small" onclick="document.getElementById('crslEditor').style.display='block';galleryImgs();$('html').css('overflow','hidden');"><i class="fas fa-edit"></i> <b>Edit Images</b></button>
      </div>
    </div>
HEREDOC;
  }
  ?>
</div>
<?php
if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
	echo <<<HEREDOC
<div id="crslEditor" class="w3-modal">
  <div class="w3-modal-content w3-animate-opacity w3-display-middle" style="margin-top:48px;max-width:90vw;margin-left:0;">
    <div class="w3-bar w3-black">
      <span onclick="document.getElementById('crslEditor').style.display='none';$('html').css('overflow','auto');" class="w3-button w3-display-topright w3-red">&times;</span>
      <span class="w3-bar-item w3-white">Gallery</span>
    </div>
    <div class="w3-panel">
      <div id="crslGallery">
        <form id="crslForm">
          <div id="crslGalleryDiv" style="max-height:50vh;height:auto;overflow:auto;" class="w3-border">
          </div>
          <input type="file" class="w3-section" name="images" accept=".png, .jpg, .jpeg"></input>
          <br />
					<span id="uploadStatus"></span>
					<br />
          <button id="crslUpload" type="submit" class="w3-btn w3-blue w3-round-small w3-card w3-left w3-section"> Upload</button>
          <button class="w3-btn w3-green w3-round-small w3-card w3-right w3-section" onclick="saveImgs();" type="button"><i class='fas fa-save'></i> Save</button>
        </form>
      </div>
    </div>
  </div>
</div>
HEREDOC;
}
?>
<script>
// Automatic Slideshow - change image every 10 seconds
var slideIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("Slides");
    for (i = 0; i < x.length; i++) {
			$( x[i] ).css("display","none");
    }
    slideIndex++;
    if (slideIndex > x.length) {slideIndex = 1} {
			$( x[(slideIndex  - 1)] ).css("display","block");
    }
    setTimeout(carousel, 10000);
}
</script>
