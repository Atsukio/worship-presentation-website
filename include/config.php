<?php
  // LOCAL configs
  define('LOCATION','/FYP/');
  define('AVATARS', LOCATION . 'avatars/');
  define('D_AVATAR', LOCATION . 'images/blank_avatar.png');
  define('CAROUSEL', LOCATION . 'images/');
  define('MAX_SIZE', 2000000);
  // ROLES
  define('R_ADMIN', 'Admin');
  define('R_ASSIST', 'Assistant');
  define('R_BROADCAST', 'Broadcast');
  define('R_EDITOR', 'Editor');
  define('R_MEMBER', 'Member');
  // COOKIES
  define('CK_ID', 'id');
  define('CK_USERNAME', 'uName');
  define('CK_TOKEN', 'token');
  define('CK_LIVE_SONG', 'wl_sg');
  define('CK_LIVE_SEQ', 'wl_seq');
  // DATABASES
  define('DB_HOST','localhost');
  define('DB_PORT','3306');
  define('DB_USER','root');
  define('DB_PASS','');
  define('DB_NAME','worship');
  // TABLES
  define('TB_SONGS','songs');
  define('TB_USERS','users');
  define('TB_PLAYLIST','playlist');
  define('TB_LIVE','live');
  define('TB_DESC', 'description');
  define('TB_CRSL', 'carousel_imgs');
  // ROLE CONTROL
  // define('RL_FULL', array(R_ADMIN, R_ASSIST));
  // define('RL_INDEX', array(R_EDITOR));
  // define('RL_BROADCAST', array(R_BROADCAST));
  // define('RL_MANAGE', array(R_ADMIN, R_ASSIST));
  // define('RL_SONGSFULL', array(R_ADMIN, R_ASSIST, R_BROADCAST));
  // define('RL_SONGSLIMIT', array(R_MEMBER));
  $RL_FULL = array(R_ADMIN, R_ASSIST);
  $RL_INDEX = array(R_EDITOR);
  $RL_BROADCAST = array(R_BROADCAST);
  $RL_MANAGE = array(R_ADMIN, R_ASSIST);
  $RL_SONGSFULL = array(R_ADMIN, R_ASSIST, R_BROADCAST);
  $RL_SONGSLIMIT = array(R_MEMBER);
?>
