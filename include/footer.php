<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-black w3-xlarge">
  <a href="https://www.facebook.com/transfornation/"><i class="fab fa-facebook-official"></i></a>
  <a href="#"><i class="fab fa-pinterest-p"></i></a>
  <a href="#"><i class="fab fa-twitter"></i></a>
  <a href="#"><i class="fab fa-flickr"></i></a>
  <a href="#"><i class="fab fa-linkedin"></i></a>
  <p class="w3-medium">
    Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a>
    <br>
    <b class="w3-tiny">Copyright &copy; <?php echo date('Y'); ?></b>
  </p>
</footer>
