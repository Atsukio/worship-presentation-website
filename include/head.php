<?php
require_once 'include/mysqli_connect.php';
?>
<!DOCTYPE html>
<html lang="en-US">
<!-- Head -->
<head>
	<title>Transfornation</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="<?php echo $const['LOCATION']; ?>images/tnc-new-logo.jpg">
	<script src="<?php echo $const['LOCATION']; ?>include/jquery-3.3.1.min.js"></script>
	<script src="<?php echo $const['LOCATION']; ?>include/pure-swipe.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $const['LOCATION']; ?>css/w3.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $const['LOCATION']; ?>css/styles.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $const['LOCATION']; ?>css/fontawesome-free-5.1.1-web/css/all.css">
