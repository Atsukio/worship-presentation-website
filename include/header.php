<!-- Navigation -->
<?php
require_once 'mysqli_connect.php';
?>
<script>

function activenav() {
	var nav = document.getElementById("navigation");
  var a = nav.getElementsByTagName("a");
	for (var i = 0; i < a.length; i++) {
		if (window.location.href.includes(a[i].href)) {
			a[i].classList.add("w3-white");
		}
	}
	var nav = document.getElementById("navTopSpan");
  var a = nav.getElementsByTagName("a");
	for (var i = 0; i < a.length; i++) {
		if (window.location.href.includes(a[i].href)) {
			a[i].classList.add("w3-white");
		}
	}
}

function nav_close() {
		$("#navigation").animate({width: 'toggle'}, "fast");
		$("#navigation").hide("fast");
		$("#navOverlay").hide();
		document.body.style.overflow = "auto";
}
function nav_open() {
    document.getElementById("navigation").style.display = "block";
		document.getElementById("navOverlay").style.display = "block";
		document.body.style.overflow = "hidden";
}
function login_check(){
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText != ''){
					document.getElementById('data_err').innerHTML = this.responseText;
				} else {
					location.reload(true);
				}
		  }
		};
		xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>login_form_check.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("username=" + document.getElementById('username').value + "&password=" + document.getElementById('password').value);
}

function signOut() {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if(this.responseText == 'success'){
				location.reload(true);
			} else {
				alert("ERROR: Failed to signout, please try again later.")
			}
		}
	};
	xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>signout.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//xmlhttp.send();
	xmlhttp.send();

}

document.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        if(document.getElementById('formlogin').style.display == 'block') {
					login_check();
				}
    }
});
</script>

<?php
if(!$validToken) {
	echo<<<HEREDOC
<div id="formlogin" class="w3-modal" style="z-index:5;">
	<form class="w3-modal-content w3-card-4 w3-display-middle w3-animate-opacity w3-mobile" style="max-width:600px;">
		<div class="w3-container">
			<div class="w3-section">
				<h2 class="w3-text-teal">Login form<span onclick="document.getElementById('formlogin').style.display='none';document.body.style.overflow='auto';" class="w3-button w3-large w3-text-black w3-hover-red w3-display-topright" title="Close Modal"><i class="fas fa-times"></i></span></h2>
				<span class="w3-text-teal">Username: </span><input class="w3-input w3-border w3-margin-bottom" type="text" id="username" name="username"  placeholder="Enter name here" required>
				<span class="w3-text-teal">Password: </span><input class="w3-input w3-border" type="password" id="password" name="password" placeholder="Enter password here" required>
				<span class="w3-text-red" id="data_err"></span>
				<br>
				<button onclick="login_check();" class="w3-button w3-block w3-green w3-section w3-padding" type="button" name="login">Login</button>
			</div>
		</div>
		<div class="w3-padding w3-light-gray">
			<button type="button" onclick="document.getElementById('formlogin').style.display='none';document.body.style.overflow='auto';" class="w3-button w3-red">Cancel</button>
		</div>
	</form>
</div>
HEREDOC;
}
?>
<div class="w3-light-green w3-top w3-card-4 w3-large" id="navTop" style="z-index:4;">
	<button id="openNav" class="w3-left w3-button w3-green w3-hide-large w3-bar-item w3-animate-opacity" onclick="nav_open()"><b><i class="fas fa-bars"></i></b></button>
	<a class="w3-left w3-button w3-text-blue w3-hover-blue w3-large w3-bar-item" href="<?php echo $const['LOCATION']; ?>index"><b>TRANSFORNATION</b></a>
	<span id="navTopSpan">
		<a href="<?php echo $const['LOCATION']; ?>index" class="w3-animate-opacity w3-left w3-button w3-bar-item w3-hide-medium w3-hide-small"><b><i class="fas fa-home"></i> Home</b></a>
	  <a href="<?php echo $const['LOCATION']; ?>music" class="w3-animate-opacity w3-left w3-button w3-bar-item w3-hide-medium w3-hide-small"><b><i class="fas fa-music"></i> Music</b></a>
	  <a href="<?php echo $const['LOCATION']; ?>worship" class="w3-animate-opacity w3-left w3-button w3-bar-item w3-hide-medium w3-hide-small"><b><i class="fas fa-hands"></i> Worship</b></a>
	  <a href="<?php echo $const['LOCATION']; ?>about" class="w3-animate-opacity w3-left w3-button w3-bar-item w3-hide-medium w3-hide-small"><b><i class="fas fa-info-circle"></i> About</b></a>
	<?php
	if($validToken){
		echo <<<HEREDOC
		<div class="w3-dropdown-hover w3-right w3-mobile">
			<button class="w3-button w3-bar-item w3-blue-gray"><img id="userAvatar" class="w3-circle" style="max-height:24px;" src="$validAvatar" alt="$validAvatar" />&nbsp;$validUser&nbsp;<i class="fa fa-caret-down"></i></button>
			<div class="w3-dropdown-content w3-bar-block w3-card-4 w3-text-black">
				<a class="w3-bar-item w3-button w3-center" href="{$const['LOCATION']}profile"><span class="w3-col s3"><i class="fas fa-user"></i></span><span class="w3-col s3"></span><span class="w3-col s6">Profile</span></a>
HEREDOC;
			if(array_intersect($RL_MANAGE,$validRoles)) {
				echo <<<HEREDOC
				<a class="w3-bar-item w3-button w3-center" href="{$const['LOCATION']}manage"><span class="w3-col s3"><i class="fas fa-cogs"></i></span><span class="w3-col s3"></span><span class="w3-col s6">Manage</span></a>
HEREDOC;
			}
echo <<<HEREDOC
				<button onclick="signOut()" class='w3-button w3-bar-item w3-mobile w3-center'><span class="w3-col s3"><i class="fas fa-sign-out-alt"></i></span><span class="w3-col s3"></span><span class="w3-col s6">Signout</span></button>
			</div>
		</div>
HEREDOC;
	} else {
		echo <<<HEREDOC
		<button onclick="document.getElementById('formlogin').style.display='block';;document.body.style.overflow='hidden';document.getElementById('username').focus();" class='w3-right w3-button w3-bar-item w3-blue-gray w3-mobile w3-center'><span class="w3-center w3-col s3"><i class='fas fa-user-circle'></i></span><span class="w3-col s3"></span><span class="w3-col s6">&nbsp;$validUser&nbsp;</span></button>
HEREDOC;
	}
	?>
	</span>
</div>
<div id="headerMargin" style="margin:43px;">
</div>
<div class="w3-overlay w3-animate-opacity" onclick="nav_close()" style="cursor:pointer;z-index:4;" id="navOverlay"></div>
<div class="w3-overlay w3-sidebar w3-bar-block w3-card w3-animate-left w3-light-green" style="width:200px;position:fixed; display:none; z-index:5;" id="navigation">
	<button class="w3-bar-item w3-button w3-mobile w3-red" onclick="nav_close()"><span class="w3-center w3-col s3"><i class="fas fa-times"></i></span><span class="w3-col s3"></span><span class="w3-col s6">Close</span></button>
	<hr class="w3-border-pale-green"/>
  <a href="<?php echo $const['LOCATION']; ?>index" class="w3-button w3-bar-item w3-mobile"><span class="w3-center w3-col s3"><i class="fas fa-home"></i></span><span class="w3-col s3"></span><span class="w3-col s6">Home</span></a>
  <a href="<?php echo $const['LOCATION']; ?>music" class="w3-button w3-bar-item w3-mobile"><span class="w3-center w3-col s3"><i class="fas fa-music"></i></span><span class="w3-col s3"></span><span class="w3-col s6">Music</span></a>
  <a href="<?php echo $const['LOCATION']; ?>worship" class="w3-button w3-bar-item w3-mobile"><span class="w3-center w3-col s3"><i class="fas fa-hands"></i></span><span class="w3-col s3"></span><span class="w3-col s6">Worship</span></a>
  <a href="<?php echo $const['LOCATION']; ?>about" class="w3-button w3-bar-item w3-mobile"><span class="w3-center w3-col s3"><i class="fas fa-info-circle"></i></span><span class="w3-col s3"></span><span class="w3-col s6">About</span></a>
</div>
<script>
   activenav();
	 $( window ).resize(function() {
	 	if($( window ).width() > 584) {
	 		document.getElementById('headerMargin').style.margin = '43px';
	 	} else {
	 		document.getElementById('headerMargin').style.margin = '86px';
	 	}
	 }).resize();

<?php
if(!$validToken) {
	echo<<<HEREDOC
	 var loginModal = document.getElementById('formlogin');

	 window.onclick = function(event) {
	 	if (event.target == loginModal) {
			document.body.style.overflow = "auto";
	 		loginModal.style.display = "none";
	 	}
	 }
HEREDOC;
}
?>
</script>
