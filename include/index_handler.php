<?php
require_once 'mysqli_connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {
  if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
    if(isset($_FILES['images'])) {
      try {
        switch ($_FILES['images']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
        }

        // You should also check filesize here.
        if ($_FILES['images']['size'] > $const['MAX_SIZE']) {
            throw new RuntimeException('Exceeded filesize limit.');
        }
      } catch (RuntimeException $e) {

          echo $e->getMessage();

      }
      $valid_extensions = array('jpeg', 'jpg', 'png'); // valid extensions

      $img = $_FILES['images']['name'];
      $tmp = $_FILES['images']['tmp_name'];
      $errorimg = $_FILES["images"]["error"];

      // get uploaded file's extension
      $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));

      $path = md5(uniqid()) . "." . $ext; // upload directory
      // check's valid format
      if(in_array($ext, $valid_extensions)) {
        if(move_uploaded_file($tmp, $_SERVER["DOCUMENT_ROOT"] . $const['CAROUSEL'] . $path)) {
          //insert form data in the database
          $sql = "INSERT INTO {$const['TB_CRSL']}
          (img_location, visible, userID) VALUES
          (?, ?, ?)";
          $ps = $mysqli->prepare($sql);
          $visible = 1;
          $ps->bind_param("sii", $path, $visible, $validId);
          if(!$ps->execute()) {
            die("error");
          } else {
            die("success");
          }
        }
      }
    }

    if(!empty($_POST['type'])) {
      switch($_POST['type']) {
        case "desc":
        $config = array (
          array('desc_title', $_POST['title']),
          array("desc_subTitle", $_POST['subtitle']),
          array("desc_desc", $_POST['desc'])
        );

        $sql = 'UPDATE ' . TB_DESC . ' SET value = ?, userID = ? WHERE name = ?';
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("sis", $desc, $validId, $name);
        foreach ($config as $value) {
          $desc = $value[1];
          $name = $value[0];
          $ps->execute();
        }
        $obj = new \stdClass();
        $obj->title = htmlspecialchars($_POST['title']);
        $obj->subtitle = htmlspecialchars($_POST['subtitle']);
        $obj->desc = htmlspecialchars($_POST['desc']);
        die(json_encode($obj));
        break;

        case "gallery":
        if(!$result = $mysqli->query("SELECT * FROM {$const['TB_CRSL']}")) {
          die("Error");
        }
        $list = "";
        while($row = $result->fetch_assoc()) {
          $list .= <<<HEREDOC
          <div class="w3-padding w3-col l3 m4 s6">
            <div class="w3-display-container">
              <img class="w3-btn w3-card" style="padding:0;height:100%;width:100%;" src="{$const['CAROUSEL']}/{$row['img_location']}" alt="{$row['img_location']}">
              <button class="w3-red w3-btn w3-card w3-display-topleft" type="button" onclick="deleteImg(this.value);" value="{$row['img_location']}"><i class="fas fa-trash-alt"></i></button>
HEREDOC;
          if($row['visible']) {
            $list .= <<<HEREDOC
            <input name="crslImgs" value="{$row['id']}" type="checkbox" class="w3-display-topright w3-check" checked />
HEREDOC;
          } else {
            $list .= <<<HEREDOC
            <input name="crslImgs" value="{$row['id']}" type="checkbox" class="w3-display-topright w3-check" />
HEREDOC;
          }
          $list .= <<<HEREDOC
            </div>
          </div>
HEREDOC;
        }
        die($list);
        break;

        case "save":
        $crslIDS = explode(",",htmlspecialchars($_POST['id']));
        $mysqli->query("UPDATE {$const['TB_CRSL']} SET visible = 0");
        $ps = $mysqli->prepare("UPDATE {$const['TB_CRSL']} SET visible = 1 WHERE id = ?");
        $ps->bind_param("s", $tempID);
        $list = '';
        foreach($crslIDS as $id) {
          $tempID = $id;
          if(!$ps->execute()) {
            die("Error");
          }
          $list .= $tempID;
        }
        die($list);
        break;

        case "delete":
        if(empty($_POST['img'])) {
          die("Error, no given file name");
        } else {
          if(!unlink($_SERVER["DOCUMENT_ROOT"] . $const['CAROUSEL'] . $_POST['img'])) {
            die("Error, failed to delete file");
          } else {
            $ps = $mysqli->prepare("DELETE FROM {$const['TB_CRSL']} WHERE img_location = ?");
            $ps->bind_param("s", $_POST['img']);
            if(!$ps->execute()) {
              die("Error, failed to remove for database");
            }
          }
        }
        break;
      }
    }
  } else {
    die("No permission");
  }
}
?>
