<?php
require_once 'config.php';

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS);
if ($mysqli->connect_error) {
	die('<br>Connect failed: ' . $mysqli->connect_error);
}

$sql = 'CREATE DATABASE IF NOT EXISTS ' . DB_NAME;
if ($mysqli->query($sql) !== TRUE) {
	die('<br>Error creating database: ' . $mysqli->error);
}

$sql = array(
	// TB_USERS Table Creation
	'CREATE TABLE IF NOT EXISTS ' . TB_USERS . ' (
		id INT(7) UNSIGNED AUTO_INCREMENT,
		username VARCHAR(16) NOT NULL,
		password CHAR(60) NOT NULL,
		token CHAR(78),
		nickname VARCHAR(45),
		email VARCHAR(320),
		roles VARCHAR(256),
		avatar VARCHAR(256),
		description varchar(4096),
		joined_date DATE,
		PRIMARY KEY (id)
	);',

	// TB_SONGS Table Creation
	'CREATE TABLE IF NOT EXISTS ' . TB_SONGS . ' (
		id INT(7) UNSIGNED AUTO_INCREMENT,
		title VARCHAR(256) NOT NULL,
		writer VARCHAR(256),
		song_no INT(7) UNSIGNED,
		song_key VARCHAR(4),
		lyrics VARCHAR(2048),
		sequence VARCHAR(256),
		transpose VARCHAR(4),
		date_modified DATETIME,
		created_by INT(7) UNSIGNED NOT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (created_by)
			REFERENCES ' . TB_USERS . ' (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);',

	// TB_PLAYLIST Table Creation
	'CREATE TABLE IF NOT EXISTS ' . TB_PLAYLIST . ' (
		id INT(7) AUTO_INCREMENT,
		song_id INT(7) UNSIGNED,
		seq_no INT(7),
		PRIMARY KEY (id),
		FOREIGN KEY (song_id)
			REFERENCES ' . TB_SONGS . ' (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);',

	// TB_LIVE Table Creation
	'CREATE TABLE IF NOT EXISTS ' . TB_LIVE . ' (
		id INT(7) UNSIGNED AUTO_INCREMENT,
		current_song INT(7) UNSIGNED,
		current_sequence INT(3) UNSIGNED,
		userID INT(7) UNSIGNED NOT NULL,
		date_modified DATETIME,
		PRIMARY KEY (id),
		FOREIGN KEY (current_song)
			REFERENCES ' . TB_SONGS . ' (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
		FOREIGN KEY (userID)
			REFERENCES ' . TB_USERS . ' (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);',

	// TB_DESC Table Creation
	'CREATE TABLE IF NOT EXISTS ' . TB_DESC . ' (
		id INT(7) UNSIGNED AUTO_INCREMENT,
		name VARCHAR(256),
		value VARCHAR(512),
		userID INT(7) UNSIGNED NOT NULL,
		date_modified DATETIME,
		PRIMARY KEY (id),
		FOREIGN KEY (userID)
			REFERENCES ' . TB_USERS . ' (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);',

	// TB_CIMG Table Creation
	'CREATE TABLE IF NOT EXISTS ' . TB_CRSL . ' (
		id INT(7) UNSIGNED AUTO_INCREMENT,
		img_location varchar(256) NOT NULL,
		visible boolean,
		userID INT(7) UNSIGNED NOT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (userID)
			REFERENCES ' . TB_USERS . ' (id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);'
);

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
// $mysqli->set_charset("utf8mb4");

// Table queries
foreach ($sql as $value) {
	if(!$mysqli->query($value)) {
		die($mysqli->error);
		die("Error: Failed to create tables");
	}
}

date_default_timezone_set('Asia/Singapore');
$date = date("Y-m-d");

// Insert an admin account
$sql = 'SELECT count(username) AS count FROM ' . TB_USERS . ' WHERE username = ?';
$ps = $mysqli->prepare($sql);
$ps->bind_param("s", $uName);
$uName = "admin";
if(!$ps->execute()) {
	die("Error: Couldn't select current accounts");
} else {
	$row = $ps->get_result()->fetch_assoc();
	if($row['count'] == 0) {
		$sql = 'INSERT INTO ' . TB_USERS . ' (username, password, roles, joined_date)
		VALUES (?, ?, ?, ?);';
		$ps = $mysqli->prepare($sql);
		$ps->bind_param("ssss", $uName, $pass, $roles,$date);
		$uName = "admin";
		$pass = '$2y$10$TYpb99s0umYI9O6q2sddRO0W/FjJr73paaUeWwzR2UZ5/yOkDz.ue';
		$roles = R_ADMIN;
		if(!$ps->execute()) {
			die("Error: Problem inserting a default account");
		}
	}
}

// Default configs
if(!$mysqli->query($sql)) {
	$config = array(
		array('desc_title', 'TRANSFORNATION'),
		array('desc_subTitle', 'Short Website Introduction'),
		array('desc_desc', 'This website will be used for viewing song lyrics, syncing the lyrics to multiple devices during broadcast and select songs to be played for worship.')
	);

	$sql = 'SELECT count(name) AS count FROM ' . TB_DESC . ' WHERE name = ?';
	$pscount = $mysqli->prepare($sql);
	$pscount->bind_param("s", $name);

	$sql = 'INSERT INTO ' . TB_DESC . '(name, value, userID, date_modified) VALUES (?, ?, 1, ?)';
	$psins = $mysqli->prepare($sql);
  $psins->bind_param("sss", $name, $desc, $date);
	$date = date("Y-m-d G:i:s");

	foreach ($config as $value) {
		$name = $value[0];
		if(!$pscount->execute()) {
			die("Error: Failed to check for description");
		}
		$row = $pscount->get_result()->fetch_assoc();
		if($row['count'] === 0) {
			$desc = $value[1];
	    if(!$psins->execute()) {
				die("Error: Failed to insert descriptions");
			}
		}
  }

	$images = array(
		"Animated-Green-Nature-Wallpaper.jpg",
		"Beautiful-Nature-Wallpapers-New-Hd.jpg",
		"wallpaper2you_187494.jpg",
		"wallpaper2you_187503.jpg",
		"nature-wallpapers-phone-For-Desktop-Wallpaper.jpg"
	);

	$result = $mysqli->query("SELECT count(id) AS count FROM " . TB_CRSL);
	$row = $result->fetch_assoc();
	if($row['count'] == 0) {
		$ps = $mysqli->prepare("INSERT INTO " . TB_CRSL . " (img_location, visible, userID) VALUES (?, 1, 1)");
		$ps->bind_param("s", $tempImg);
		foreach($images AS $img) {
			$tempImg = $img;
			if(!$ps->execute()) {
				die("Failed to upload default images");
			}
		}
	}


} else {
	die("Error executing queries");
}
?>
