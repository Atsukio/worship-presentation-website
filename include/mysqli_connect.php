<?php
require_once 'config.php';
$const = get_defined_constants();

$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if($mysqli->connect_error) {
  exit('Error connecting to database'); //Should be a message a typical user could understand in production
}
// $mysqli->set_charset("utf8mb4");

$validToken = false;
$validUser = "Login";
$validRoles = array();
if(!empty($_COOKIE[CK_TOKEN])) {
	$sql = "SELECT id, roles, avatar, username FROM " . TB_USERS . " WHERE token = ?";
	$ps = $mysqli->prepare($sql);
	$ps->bind_param("s", $_COOKIE[CK_TOKEN]);
	if(!$ps->execute()) {
		die("ERROR: Problem occured while finding user");
	}
	$row = $ps->get_result()->fetch_assoc();
	if(!empty($row['id'])) {
		$validToken = true;
		$validRoles = explode(",", $row['roles']);
    $validId = $row['id'];
		if(empty($row['avatar'])) {
			$validAvatar = $const['D_AVATAR'] . "?" . time();
      $unlinkAvatar = false;
		} else {
			$validAvatar = htmlspecialchars($const['AVATARS'] . $row['avatar'] . "?" . time());
      $unlinkAvatar = htmlspecialchars($const['AVATARS'] . $row['avatar']);
    }
    $validUser = $row['username'];
	} else {
    if(!$_COOKIE[CK_TOKEN]) {
      unset($_COOKIE[CK_TOKEN]);
    }
  }
}
?>
