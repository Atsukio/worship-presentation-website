<?php
require_once 'mysqli_connect.php';

if($_SERVER['REQUEST_METHOD'] == "POST" && !empty($_COOKIE[CK_TOKEN])) {
  if(isset($_POST['username']) && empty($_POST['type'])) {
    if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
      if(!empty($_POST['username'])) {
        $sql = "SELECT avatar, username, nickname, roles
        FROM {$const['TB_USERS']}
        WHERE username LIKE ?";
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("s", $usr);
        $usr = "%{$_POST['username']}%";
      } else {
        $sql = "SELECT avatar, username, nickname, roles
        FROM {$const['TB_USERS']}";
        $ps = $mysqli->prepare($sql);
      }
      if(!$ps->execute()) {
        die("ERROR: Something went wrong while searching for users");
      } else {
        $result = $ps->get_result();
        $list = '';
        while($row = $result->fetch_assoc()) {
          if(empty($row['avatar'])) {
            $avatar = $const['D_AVATAR'];
            $avatarAlt = "blank_avatar";
          } else {
            $avatar = htmlspecialchars($const['AVATARS'] . $row['avatar']);
            $avatarAlt = htmlspecialchars($row['avatar']);
          }
          $username = htmlspecialchars($row['username']);
          $nickname = htmlspecialchars($row['nickname']);
          $roles = htmlspecialchars($row['roles']);
          $list .= <<<HEREDOC
          <div style="cursor:pointer;" onclick="showProfile('{$row['username']}');$('#updateStatus').html('');" class="w3-margin-top w3-col w3-border w3-border-black">
            <div class="w3-col l1 m2 s3 w3-section">
              <img class="w3-image w3-container w3-border w3-round w3-margin-left" style="padding:8px;" src="{$avatar}" alt="{$avatarAlt}" />
            </div>
            <div class="w3-col l11 m10 s9 w3-margin-top" style="padding-left:24px;">
              <span class="w3-text-blue w3-large" style="font-weight:bold;">{$username}</span><br />
HEREDOC;
            if(!empty($nickname)) {
          $list .= <<<HEREDOC
              <span class="w3-text-dark-gray w3-small">({$nickname})</span><br />
HEREDOC;
            }
          $list .= <<<HEREDOC
              <span class="w3-text-black w3-small">Roles:<b>{$roles}</b></span><br />
            </div>
          </div>
HEREDOC;
        }
        if(!$list) {
          die("<h2 class='w3-text-red'>No users found with this username.</h2>");
        } else {
          die($list);
        }
      }
    } else {
      die("<h2 class='w3-text-red'>No permission to view this page</h2>");
    }
  } else if(!empty($_POST['profile'])) {
    if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
      $profile = new \stdClass();
      $sql = "SELECT avatar, username, nickname, roles, joined_date, email
      FROM {$const['TB_USERS']}
      WHERE username = ?";
      $ps = $mysqli->prepare($sql);
      $ps->bind_param("s", $_POST['profile']);
      if(!$ps->execute()) {
        $profile->status = "ERROR: Something went wrong while searching for users";
        $JSON = json_encode($profile);
        die($JSON);
      } else {
        $row = $ps->get_result()->fetch_assoc();
        if(empty($row['username'])) {
          $profile->status = "ERROR: Invalid username, please refresh the page.";
          $JSON = json_encode($profile);
          die($JSON);
        } else {
          $profile->status = "success";
          if(empty($row['avatar'])) {
            $profile->avatar = htmlspecialchars($const['D_AVATAR']);
            $profile->avatarAlt = "blank_avatar";
          } else {
            $profile->avatar = htmlspecialchars($const['AVATARS'] . $row['avatar']);
            $profile->avatarAlt = htmlspecialchars($row['avatar']);
          }
          $profile->username = htmlspecialchars($row['username']);
          $profile->nickname = htmlspecialchars($row['nickname']);
          $profile->roles = htmlspecialchars($row['roles']);
          $profile->date = htmlspecialchars($row['joined_date']);
          $profile->email = htmlspecialchars($row['email']);
          $JSON = json_encode($profile);
          die($JSON);
        }
      }
    } else {
      $profile->status = "No permission to manage user's profile";
      $JSON = json_encode($profile);
      die($JSON);
    }
  } else if(!empty($_POST['type'])) {
    if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_MANAGE,$validRoles)) {
      if($_POST['type'] == "updateRolesForm") {
        if(!empty($_POST['password'])) {
          $sql = "UPDATE {$const['TB_USERS']}
          SET password = ?, roles = ?
          WHERE username = ?";
          $ps = $mysqli->prepare($sql);
          $pass = password_hash($_POST['password'],PASSWORD_DEFAULT);
          $ps->bind_param("sss", $pass, $_POST['roles'], $_POST['username']);
        } else {
          $sql = "UPDATE {$const['TB_USERS']}
          SET roles = ?
          WHERE username = ?";
          $ps = $mysqli->prepare($sql);
          $ps->bind_param("ss", $_POST['roles'], $_POST['username']);
        }
        if(!$ps->execute()) {
          die("<h4 class='w3-text-red'>Failed to save changes</h4>");
        } else {
          die("<h4 class='w3-text-green'>Changes saved successfully.</h4>");
        }
      } else if($_POST['type'] == "registerForm") {
        if(empty($_POST['username']) || empty($_POST['password'])) {
          die("<h4 class='w3-text-red'>Username / Password cannot be empty</h4>");
        } else if(!empty($_POST['username']) && !preg_match("/^[a-zA-Z0-9_]*$/",$_POST['username'])) {
  				echo "Only alphanumeric symbols and underscore allowed";
        } else {
          //SAM'S PART || START
          $sql = "SELECT username FROM {$const['TB_USERS']}
                  WHERE username = ?";
          $ps = $mysqli->prepare($sql);
          $ps->bind_param("s", $_POST['username']);
          if(!$ps->execute()){
            die("<h4 class='w3-text-red'>There is an error in checking the database</h4>");
          }
          $ps->execute();
          $row = $ps->get_result()->fetch_assoc();
          if(!empty($row['username'])){
            die("<h4 class='w3-text-red'>This username has been taken.</h4>");
          }
          //SAM'S PART || END
          $sql = "INSERT INTO {$const['TB_USERS']}
          (username, password, roles, joined_date)
          VALUES (?, ?, ?, ?)";
          date_default_timezone_set('Asia/Singapore');
          $date = date("Y-m-d");
          $ps = $mysqli->prepare($sql);
          $pass = password_hash($_POST['password'],PASSWORD_DEFAULT);
          $ps->bind_param("ssss", $_POST['username'], $pass, $_POST['roles'], $date);
          if(!$ps->execute()) {
            die("<h4 class='w3-text-red'>Failed to register account</h4>");
          } else {
            die("<h4 class='w3-text-green'>Registered successfully.</h4>");
          }
        }
      } else if($_POST['type'] == "usernameCheck") {
        if(empty($_POST['username'])) {
          die("");
        } else {
          $sql = "SELECT username
          FROM {$const['TB_USERS']}
          WHERE username = ?";
          $ps = $mysqli->prepare($sql);
          $ps->bind_param("s", $_POST['username']);
          if(!$ps->execute()) {
            die("Error: Failed to search for usernames");
          } else {
            $row = $ps->get_result()->fetch_assoc();
            if(!empty($row['username'])) {
              die("<span class='w3-text-red'><b>Exists</b></span>");
            } else {
              die("<span class='w3-text-green'><b>Available</b></span>");
            }
          }
        }
      } else if($_POST['type'] == "deleteUser") {
        $sql = "DELETE FROM {$const['TB_USERS']} WHERE username = ?";
        $ps = $mysqli->prepare($sql);
        $ps->bind_param("s",$_POST['username']);
        if(!$ps->execute()) {
          die("Error: Failed to delete user account.");
        } else {
          die("success");
        }
      }
    } else {
      die("<h4 class='w3-text-red'>No permission to view this page</h4>");
    }
  } else if(empty($row['id'])) {
    die("<b class='w3-text-red'>ERROR: Invalid user account.</b>");
  } else if(isset($_POST['cPass']) && !empty($_POST['cPass']) && isset($_POST['nPass']) && empty($_POST['nPass'])) {
    die("<b class='w3-text-red'>ERROR: New password cannot be empty.</b>");
  } else if(isset($_POST['nPass']) && !empty($_POST['nPass']) && !isset($_POST['cPass']) && empty($_POST['cPass'])) {
    die("<b class='w3-text-red'>ERROR: Current password cannot be empty.</b>");
  } else if(!empty($_FILES["avatar"]["name"])) {
    $valid_extensions = array('jpeg', 'jpg', 'png'); // valid extensions

    if(isset($_FILES['avatar'])) {
      try {
        switch ($_FILES['avatar']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
        }

        // You should also check filesize here.
        if ($_FILES['avatar']['size'] > $const['MAX_SIZE']) {
            throw new RuntimeException('Exceeded filesize limit.');
        }
      } catch (RuntimeException $e) {

          echo $e->getMessage();

      }
      $img = $_FILES['avatar']['name'];
      $tmp = $_FILES['avatar']['tmp_name'];
      $errorimg = $_FILES["avatar"]["error"];

      // get uploaded file's extension
      $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));

      $path = "$validUser" . "." . $ext; // upload directory
      // check's valid format
      if(in_array($ext, $valid_extensions)) {
        if(move_uploaded_file($tmp, $_SERVER["DOCUMENT_ROOT"] . $const['AVATARS'] . $path)) {
          echo htmlspecialchars("$path");
          if($const['AVATARS'] . $path !== $unlinkAvatar && $unlinkAvatar !== false) {
            if(!unlink($_SERVER["DOCUMENT_ROOT"] . $unlinkAvatar)) {
              die("Error, failed to delete file");
            }
          }
        //insert form data in the database
          $sql = "UPDATE {$const['TB_USERS']}
          SET avatar = ?
          WHERE id = {$validId}";
          $ps = $mysqli->prepare($sql);
          $ps->bind_param("s", $path);
          if(!$ps->execute()) {
            die("error");
          }
        }
      } else {
        die('invalid');
      }
    }
  } else if(isset($_POST['cPass']) && !empty($_POST['cPass']) &&
    isset($_POST['nPass']) && !empty($_POST['nPass'])) {

    $sql = "SELECT password FROM users WHERE token = ?";
    $ps = $mysqli->prepare($sql);
    $ps->bind_param("s", $_COOKIE[CK_TOKEN]);
    if(!$ps->execute()){
      die("Failed to check current password. Try again later.");
    }

    $row = $ps->get_result()->fetch_assoc();
    $pass = $row['password'];

    if(password_verify($_POST['cPass'], $pass)) {
      $sql = "UPDATE " . TB_USERS . "
      SET nickname = ?, email = ?, password = ?, description = ?
      WHERE token = ? ";
      $ps = $mysqli->prepare($sql);
      $newPass = password_hash($_POST['nPass'],PASSWORD_DEFAULT);
      $ps->bind_param("sssss", $_POST['nName'], $_POST['email'], $newPass, $_POST['description'], $_COOKIE[CK_TOKEN]);
      if(!$ps->execute()) {
        die("<b class='w3-text-red'>ERROR: Failed to save, please try again later.</b>");
      } else {
        die("<b class='w3-text-green'>Saved successfully.</b");
      }
    } else {
      die("<b class='w3-text-red'>ERROR: Invalid current password.</b>");
    }
  } else {
    $sql = "UPDATE " . TB_USERS . "
    SET nickname = ?, email = ?, description = ?
    WHERE id = " . $row['id'];
    $ps = $mysqli->prepare($sql);
    $ps->bind_param("sss", $_POST['nName'], $_POST['email'], $_POST['description']);
    if(!$ps->execute()) {
      die("<b class='w3-text-red'>ERROR: Failed to save, please try again later.</b>");
    } else {
      die("<b class='w3-text-green'>Saved successfully.</b");
    }
  }
}
?>
