<span class="w3-text-teal"><b>Roles</b></span>
<span class="w3-tooltip">
  <span class="w3-text-teal"><b><i class="fas fa-info-circle"></i></b></span>
  <span class="w3-text-blue"><b>: <?php if(!empty($ppRoles)){echo $ppRoles;} ?></b></span>
  <br />
  <table style="position:relative;left:0;top:-5px;;font-weight:bold;z-index:1;table-layout:auto;width:auto;"
  class="w3-left-align w3-left w3-text w3-animate-opacity w3-padding w3-table w3-bordered w3-text-blue w3-black">
  <tr><td><?php echo $const['R_ADMIN']; ?></td><td>: </td><td>Full access</td></tr>
  <tr><td><?php echo $const['R_ASSIST']; ?></td><td>: </td><td>Almost full access</td></tr>
  <tr><td><?php echo $const['R_BROADCAST']; ?></td><td>: </td><td>Manage songs and broadcast</td></tr>
  <tr><td><?php echo $const['R_EDITOR']; ?></td><td>: </td><td>Edits Homepage</td></tr>
  <tr><td><?php echo $const['R_MEMBER']; ?></td><td>: </td><td>Manages their own songs</td></tr>
  <tr><td>Empty</td><td>: </td><td>Basic access</td></tr>
  </table>
</span>
