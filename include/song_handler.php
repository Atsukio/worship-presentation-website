<?php
require_once 'mysqli_connect.php';

if($_SERVER['REQUEST_METHOD'] == "POST") {
	if(isset($_POST['input']) && !empty($_POST['input'])) {
		switch($_POST['input']) {
			case "check":
			if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_SONGSLIMIT,$validRoles)) {
				if(isset($_POST["title"]) && !empty($_POST["title"])) {
					$sql = "SELECT count(title) AS count
					FROM " . TB_SONGS .
					" WHERE title = ?";
					$ps = $mysqli->prepare($sql);
					$ps->bind_param("s", $_POST['title']);
					if(!$ps->execute()) {
						die("Error: Couldn't search through the song list");
					}
					$row = $ps->get_result()->fetch_assoc();
					if($row['count'] === 0) {
						die('<b>Song Title: </b><b class="w3-text-green">Available</b>');
					} else {
						die('<b>Song Title: </b><b class="w3-text-red">Exists</b>');
					}
				}
			} else {
				die("Error: No permission");
			}
			break;

			case "delete":
			if(empty($_POST["id"])) {
				die("Error: Song selection is empty");
			} else if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_SONGSLIMIT,$validRoles)) {
				if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
					$sql = "SELECT u.username AS username
					FROM {$const['TB_USERS']} u, {$const['TB_SONGS']} s
					WHERE u.id = s.created_by AND u.token = ? AND s.id = ?";
					$ps = $mysqli->prepare($sql);
					$ps->bind_param("si", $_COOKIE[CK_TOKEN], $_POST['id']);
					if(!$ps->execute()) {
						die("Error: Searching for user songs");
					} else {
						$row = $ps->get_result()->fetch_assoc();
						if(empty($row['username'])) {
							die("Error: Song doesn't belong to you");
						}
					}
				}
			} else {
				die("Error: No permission");
			}
			$sql = "DELETE FROM " . TB_SONGS . " WHERE id = ?";
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("i", $_POST['id']);
			if(!$ps->execute()) {
				die("Error: Song failed to be deleted");
			}
			if($ps->affected_rows === 0) {
				die("Error: Song doesn't exist");
			} else {
				die('<b class="w3-text-green">Song successfully deleted.</b>');
			}
			break;

			case "save":
			if(empty($_POST["title"])) {
				die("Error: Song title cannot be empty");
			} else if(empty($_POST['id'])) {
				die("Error: Invalid song selection");
			} else if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_SONGSLIMIT,$validRoles)) {
				if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
					$sql = "SELECT u.username AS username
					FROM {$const['TB_USERS']} u, {$const['TB_SONGS']} s
					WHERE u.id = s.created_by AND u.token = ? AND s.id = ?";
					$ps = $mysqli->prepare($sql);
					$ps->bind_param("si", $_COOKIE[CK_TOKEN], $_POST['id']);
					if(!$ps->execute()) {
						die("Error: Searching for user songs");
					} else {
						$row = $ps->get_result()->fetch_assoc();
						if(empty($row['username'])) {
							die("Error: Song doesn't belong to you");
						}
					}
				}
			} else {
				die("Error: No permission");
			}
			date_default_timezone_set('Asia/Singapore');
			$sql = 'UPDATE ' . TB_SONGS .
			' SET title = ?, writer = ?, song_no = ?,' .
			'song_key = ?, transpose = ?, lyrics = ?,' .
			'sequence = ?, date_modified = ? WHERE id = ?';
			$date = date('Y-m-d G:i:s');
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("ssisssssi", $_POST['title'], $_POST['writer'],
			$_POST['songno'], $_POST['songkey'], $_POST['transpose'],
			$_POST['lyrics'], $_POST['sequence'], $date, $_POST['id']);
			if(!$ps->execute()) {
				die("Error: Song failed to be saved");
			}
			die('<b class="w3-text-green">Song successfully saved.</b>');
			break;

			case "retrievelist":
			$sql = "SELECT id, title, song_key, lyrics FROM " . TB_SONGS .
			" ORDER BY title";
			if(!$result = $mysqli->query($sql)) {
				die("Error: Something went wrong while retrieving song list");
			}
			$list = '';
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$list .= '<li value="' . $row['id'] . '" style="overflow: hidden; text-overflow: ellipsis; overflow-y: hidden;">' . $row['title'];
				if ($row['song_key'] != '' && strpos($row['lyrics'], '♮') != false) {
					$list .= ' <i class="fas fa-check-square w3-text-green"></i>';
				}
				$list .= '</li>';
			}
			die($list);
			break;

			case "retrievelistMember":
			if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
				$sql = "SELECT s.id AS id, s.title AS title,
				s.song_key AS song_key, s.lyrics AS lyrics
				FROM {$const['TB_SONGS']} s, {$const['TB_USERS']} u
				WHERE s.created_by = u.id AND u.token = ?
				ORDER BY title";
				$ps = $mysqli->prepare($sql);
				$ps->bind_param("s", $_COOKIE[CK_TOKEN]);
				if(!$ps->execute()) {
					die("Error: Failed to check for valid user account");
				} else {
					$result = $ps->get_result();
					$list = '';
					// output data of each row
					while($row = $result->fetch_assoc()) {
						$list .= '<li value="' . $row['id'] . '" style="overflow: hidden; text-overflow: ellipsis; overflow-y: hidden;">' . $row['title'];
						if ($row['song_key'] != '' && strpos($row['lyrics'], '♮') != false) {
							$list .= ' <i class="fas fa-check-square w3-text-green"></i>';
						}
						$list .= '</li>';
					}
					die($list);
				}
			} else {
				die("Error: No permission");
			}
			break;

			case "select":
			if(isset($_POST["id"]) && !empty($_POST["id"])) {
				$sql = "SELECT title, writer, song_no, song_key, transpose, lyrics, sequence FROM " . TB_SONGS . " WHERE id = ?";
				$ps = $mysqli->prepare($sql);
				$ps->bind_param("i", $_POST["id"]);
				if(!$ps->execute()) {
					die("Error: Song couldn't be selected");
				}
				$row = $ps->get_result()->fetch_assoc();
				die(
					$row['title'] . "~split~" . $row['writer'] . "~split~" .
					$row['song_no'] . "~split~" . $row['song_key'] . "~split~" .
					$row['transpose'] . "~split~" . $row['lyrics'] . "~split~" . $row['sequence']
				);
			}
			break;

			case "submit":
			if(empty($_POST["title"])) {
				die("Error: Song title cannot be empty");
			} else if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_SONGSLIMIT,$validRoles)) {
				$sql = "SELECT count(title) AS count FROM " . TB_SONGS . " WHERE title = ?";
				$ps = $mysqli->prepare($sql);
				$ps->bind_param("s", $_POST["title"]);
				if(!$ps->execute()) {
					die("Error: Song verification failed");
				}
				$row = $ps->get_result()->fetch_assoc();
				if($row['count'] > 0) {
					die('<b class="w3-text-red">Failed to submit, song already exists.</b>');
				}
				date_default_timezone_set('Asia/Singapore');
				$sql = 'INSERT INTO ' . TB_SONGS .
				' (title, writer, song_no, song_key, transpose, lyrics, sequence, date_modified, created_by) ' .
				' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
				$date = date('Y-m-d G:i:s');
				$ps = $mysqli->prepare($sql);
				$ps->bind_param("ssisssssi", $_POST['title'], $_POST['writer'],
				$_POST['songno'], $_POST['songkey'], $_POST['transpose'],
				$_POST['lyrics'], $_POST['sequence'], $date, $validId);
				if(!$ps->execute()) {
					die("Error: Song failed be submitted");
				}
				die('<b class="w3-text-green">Song successfully submitted.</b>');
			} else {
				die("Error: No permission");
			}
			break;
		}
	}
}
?>
