<br>
<div class="w3-row-padding">
	<div class="w3-col l4 m4 s6 w3-panel">
		<!-- Show Chords -->
		<input type="checkbox" class="w3-check" id="showchords" onclick="slideControl(0)" <?php if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {echo "checked";} ?>>
		<label class="w3-text-blue" style="font-size:16px; font-weight:bold;" for="showchords">Show chords</label>
	</div>
	<div class="w3-col l4 m4 s6 w3-panel">
		<!-- Scroll Mode -->
		<input type="checkbox" class="w3-check" id="scrollmode" onclick="slideControl(0)">
		<label class="w3-text-dark-gray" style="font-size:16px; font-weight:bold;" for="scrollmode">Scroll mode</label>
	</div>
	<div class="w3-col l4 m4 s6 w3-panel">
		<button class="w3-btn w3-white w3-border w3-border-blue w3-hover-blue w3-round w3-card" onclick="printSheet();"><i class="fas fa-print"></i> Print this sheet</button>
	</div>
</div>
<div class="w3-panel w3-card-4 w3-display-container w3-light-gray" style="min-height:75vh;" id="fullscreensheet">
	<div class="w3-display-topmiddle" style="width:100%; padding-top:8%; padding-bottom:8%; padding-left:20%; padding-right:20%; height:100%; overflow-y:auto;">
		<h2 id="sheetlyrics" style="font-size:2vw;"></h2>
	</div>
	<a class="w3-display-left w3-display-hover w3-animate-opacity w3-hover-light-grey" id="prevslidebtn" style="font-size:4vw; cursor:pointer; display:none;" onclick="slideControl(-1)">&#10094;</a>
	<a class="w3-display-right w3-display-hover w3-animate-opacity w3-hover-light-grey" id="nextslidebtn" style="font-size:4vw; cursor:pointer; display:none;" onclick="slideControl(1)">&#10095;</a>
	<h4 class="w3-display-topleft w3-panel"><div id="songdesc" style="font-size:1.5vw;font-weight:bold;"></div><div id="writerinfo" style="font-size:1.0vw;"></div></h4>
	<label class="w3-display-topright w3-panel" onclick="toggleFullScreen();" style="cursor:pointer; font-size:1.5vw;" id="fullscreenbtn"><i class="fas fa-expand-arrows-alt"></i></label>
	<h6 class="w3-display-bottomright w3-panel" id="seqinfo" style="font-size:1.5vw; font-weight:bold;"></h6>
</div>
