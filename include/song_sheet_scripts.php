<!-- Embedded Styles START -->
<style>
#fullscreensheet:-webkit-full-screen {
  width: 100%;
  height: 100%;
  margin: 0;
}

li {
	overflow: hidden;
}
</style>
<!-- Embedded Styles END -->
<script>
<!-- Global variables START -->
var screenWidth;

var song = {
	id: -1,
	title:"",
	writer:"",
	no:"",
	key:"",
	transpose:"",
	lyrics:""
};

var sheet = {
	currentSequence:0,
	sequence:[],
	lyrics:[]
};

var keys = [
  <!-- Major Keys START -->
  ["C","C","D","E","F","G","A","B"],
  ["Db","Db","Eb","F","Gb","Ab","Bb","C"],
  ["D","D","E","F#","G","A","B","C#"],
  ["Eb","Eb","F","G","Ab","Bb","C","D"],
  ["E","E","F#","G#","A","B","C#","D#"],
  ["F","F","G","A","Bb","C","D","E"],
  ["Gb","Gb","Ab","Bb","Cb","Db","Eb","F"],
  ["G","G","A","B","C","D","E","F#"],
  ["Ab","Ab","Bb","C","Db","Eb","F","G"],
  ["A","A","B","C#","D","E","F#","G#"],
  ["Bb","Bb","C","D","Eb","F","G","A"],
  ["B","B","C#","D#","E","F#","G#","A#"],
  <!-- Major Keys END -->
  <!-- Minor Keys START -->
  ["Am","A","B","C","D","E","F","G"],
  ["Bbm","Bb","C","Db","Eb","F","Gb","Ab"],
  ["Bm","B","C#","D","E","F#","G","A"],
  ["Cm","C","D","Eb","F","G","Ab","Bb"],
  ["C#m","C#","D#","E","F#","G#","A","B"],
  ["Dm","D","E","F","G","A","Bb","C"],
  ["Ebm","Eb","F","Gb","Ab","Bb","Cb","Db"],
  ["Em","E","F#","G","A","B","C","D"],
  ["Fm","F","G","Ab","Bb","C","Db","Eb"],
  ["F#m","F#","G#","A","B","C#","D","E"],
  ["Gm","G","A","Bb","C","D","Eb","F"],
  ["G#m","G#","A#","B","C#","D#","E","F#"]
  <!-- Minor Keys END -->
];
<!-- Global variables END -->
document.onfullscreenchange = function ( event ) {
	if (document.getElementById('fullscreenbtn').innerHTML == '<b><i class="fas fa-compress"></i></b>') {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-expand-arrows-alt"></i></b>';
	} else {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-compress"></i></b>';
	}
};
document.onwebkitfullscreenchange = function ( event ) {
	if (document.getElementById('fullscreenbtn').innerHTML == '<b><i class="fas fa-compress"></i></b>') {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-expand-arrows-alt"></i></b>';
	} else {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-compress"></i></b>';
	}
};
document.onmozfullscreenchange = function ( event ) {
	if (document.getElementById('fullscreenbtn').innerHTML == '<b><i class="fas fa-compress"></i></b>') {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-expand-arrows-alt"></i></b>';
	} else {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-compress"></i></b>';
	}
};
document.MSFullscreenChange = function ( event ) {
	if (document.getElementById('fullscreenbtn').innerHTML == '<b><i class="fas fa-compress"></i></b>') {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-expand-arrows-alt"></i></b>';
	} else {
		document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-compress"></i></b>';
	}
};

function resizeSongList() {
  var height = ($('#content').height());
  $('#slSidebar').height(height);
  $('#songlist').css({"max-height":(height - 140)});
}

function printSheet() {
	var printWindow=window.open('','','fullscreen=yes');
	var tempLyrics = '';
	if (sheet.sequence == '') {
		tempLyrics += sheet.lyrics;
	} else {
		for (var i = 0; i < sheet.lyrics.length; i++) {
			tempLyrics += sheet.lyrics[i];
			if (i < (sheet.lyrics.length - 1)) {
				tempLyrics += "<br><br>";
			}
		}
	}

	printWindow.document.body.innerHTML += '<div style="overflow:auto;">';

	if (document.getElementById("showchords").checked) {
			if (song.key != "") {
				if (song.transpose != "") {
					printWindow.document.body.innerHTML += '<span style="font-size:1.5em;font-weight:bold;">' + song.title + "</span><span style='color:gray;> [KEY: " + song.transpose + "]</span>" + "<br><span style='font-size:1em;'>" + song.writer + "</span>";
				} else {
					printWindow.document.body.innerHTML += '<span style="font-size:1.5em;font-weight:bold;">' + song.title + "</span><span style='color:gray;> [KEY: " + song.key + "]</span>" + "<br><span style='font-size:1em;'>" + song.writer + "</span>";
				}
				tempLyrics = tempLyrics.replace(/display:none;/g, "");
			}
	} else {
		printWindow.document.body.innerHTML += '<span style="font-size:1.5em;font-weight:bold;">' + song.title + "</span><br><span style='font-size:1em;'>" + song.writer + "</span>";
	}

	printWindow.document.body.innerHTML += `<br><br><br><div style="margin:auto; width:60%; font-size:1.5em;">`
	+ tempLyrics +
	`</div>`;
	printWindow.document.close();
	printWindow.focus();
	printWindow.print();
	printWindow.close();
	return false;
}

<!-- Fullscreen function START -->
function toggleFullScreen() {
	var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
        (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
        (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
        (document.msFullscreenElement && document.msFullscreenElement !== null);

    var docElm = document.getElementById("fullscreensheet");
    if (!isInFullScreen) {
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        } else if (docElm.msRequestFullscreen) {
            docElm.msRequestFullscreen();
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}
<!-- Fullscreen function END -->

<!-- Sheet lyrics START -->
function sheetLyrics() {
  var tempLyrics = sheet.lyrics.split("<br>");
  var lyrics = tempLyrics[0] + "<br>";
	if (song.transpose != "" && Object.is(song.key, song.transpose) == false) {
    var currentKey = "", transposeKey = "";
		for (var i = 0; i < keys.length; i++) {
			if (keys[i][0].toUpperCase() == song.key.toUpperCase()) {
				currentKey = i;
			} else if (keys[i][0].toUpperCase() == song.transpose.toUpperCase()) {
				transposeKey = i;
			}
			if (currentKey != "" && transposeKey != "") {
				break;
			}
		}
		for (var i = 1; i < tempLyrics.length; i++) {
			if (tempLyrics[i].includes("[♮]")) {
        for (var i2 = 1; i2 < keys[currentKey].length; i2++) {
          tempLyrics[i] = tempLyrics[i].replace(new RegExp(keys[currentKey][i2], "g"), i2);
        }
        for (var i2 = 1; i2 < keys[transposeKey].length; i2++) {
          tempLyrics[i] = tempLyrics[i].replace(new RegExp(i2, "g"), keys[transposeKey][i2]);
        }
      }
		}
	}
  for (var i = 1; i < tempLyrics.length; i++) {
    if (tempLyrics[i].includes("[♮]")) {
      tempLyrics[i] = '<span class="w3-text-grey" style="display:none;">' + tempLyrics[i].replace(/ /g, "&nbsp;") + '<br></span>';
      tempLyrics[i] = tempLyrics[i].replace(/\[♮]/g, '');
    } else {
      tempLyrics[i] = tempLyrics[i] + "<br>";
    }
    lyrics += tempLyrics[i];
  }

  sheet.lyrics = lyrics.replace(/<br><br>/g, "<br>");

  if(sheet.sequence == '') {
    var listItem = ["1","2","3","4","5","6","7","8","9","10",
    "prechorus","chorus","bridge","prechorus 2","chorus 2","bridge 2","ending"];
    tempLyrics = sheet.lyrics.split("[");
    if(tempLyrics[1] !== undefined) {
      var tempCheck, tempSeq = 0;
      for(var i = 1; i < tempLyrics.length; i++) {
        tempCheck = tempLyrics[i].split("]");
        item = '';
        if(listItem.indexOf(tempCheck[0]) != -1) {
          for (var i2 = 1; i2 <= 10; i2++) {
            if (tempCheck[0] == i2) {
              item = i2;
            }
          }
          if (tempCheck[0] == "prechorus") {
            item = "p";
          } else if (tempCheck[0] == "chorus") {
            item = "c";
          } else if (tempCheck[0] == "bridge") {
            item = "b";
          } else if (tempCheck[0] == "prechorus 2") {
            item = "q";
          } else if (tempCheck[0] == "chorus 2") {
            item = "t";
          } else if (tempCheck[0] == "bridge 2") {
            item = "w";
          } else if (tempCheck[0] == "ending") {
            item = "e";
          }
          if(item != '') {
            sheet.sequence[tempSeq] = item;
            tempSeq++;
          }
        }
      }
    }
  }
  if (sheet.sequence != '') {
    sheet.lyrics = [];
  	for (var i = 0; i < sheet.sequence.length; i++) {
      var item = "";
  		if (sheet.sequence[i] == "p") {
  			item = "prechorus";
  		} else if (sheet.sequence[i] == "c") {
  			item = "chorus";
  		} else if (sheet.sequence[i] == "b") {
  			item = "bridge";
  		} else if (sheet.sequence[i] == "q") {
  			item = "prechorus 2";
  		} else if (sheet.sequence[i] == "t") {
  			item = "chorus 2";
  		} else if (sheet.sequence[i] == "w") {
  			item = "bridge 2";
  		} else if (sheet.sequence[i] == "e") {
  			item = "ending";
  		} else {
        for (var i2 = 1; i2 <= 10; i2++) {
    			if (sheet.sequence[i] == i2) {
    				item = i2;
            break;
    			}
    		}
      }
  		tempLyrics = lyrics.split("[" + item + "]<br>");
  		tempLyrics = tempLyrics[1].split("<br>[");
      if (isNaN(Number(item))) {
  			item = item.slice(0,1).toUpperCase() + item.slice(1);
  		}
      if (item == "Chorus" || item == "Chorus 2" || item == "Prechorus" || item == "Prechorus 2") {
        sheet.lyrics[i] = "<i><u class='w3-text-blue'>" + item + ":</u><br>" + tempLyrics[0] + "</i>";
      } else {
        sheet.lyrics[i] = "<u class='w3-text-blue'>" + item + ":</u><br>" + tempLyrics[0];
      }
    }
  }
}

<!-- Sheet lyrics END -->

function open_sl() {
    document.getElementById("slSidebar").style.display = "block";
    document.getElementById("slSidebar").style.position = "fixed";
    document.getElementById("slOverlay").style.display = "block";
    document.getElementById("navTop").style.zIndex = 2;
}
function close_sl() {
    $("#slSidebar").animate({width: 'toggle'}, "fast");
    $("#slSidebar").hide("fast");
    $("#navOverlay").hide();
    document.getElementById("slSidebar").style.position = "absolute";
    document.getElementById("slOverlay").style.display = "none";
    document.getElementById("navTop").style.zIndex = 4;
}

<!-- When document loads finish -->
$(document).ready(function(){
	// On key press
	$(document).keydown(function(event){
		// If Music sheet tab is visible
		if ($('#musicsheettab').is(':visible')) {
			// Left arrow key is pressed
			if (event.which == 37) {
				slideControl(-1);
			// Right arrow key is pressed
			} else if (event.which == 39) {
				slideControl(1);
			}

			if (event.which == 27 || event.which == 122) {
				document.getElementById('fullscreenbtn').innerHTML = '<b><i class="fas fa-expand-arrows-alt"></i></b>';
			}
		}
	});

  $( window ).resize(function() {
    if($( window ).width() > 600 && document.getElementById('slSidebar').style.display == 'block') {
      close_sl();
    }
    resizeSongList();
    if($( window ).width() > 992) {
      screenWidth = 1;
    } else if($( window ).width() > 600) {
      screenWidth = 1.5;
    } else {
      screenWidth = 2;
    }
    $( "#sheetlyrics" ).css("font-size", (screenWidth * 2) + "vw");
    $( "#prevslidebtn, #nextslidebtn" ).css("font-size", (screenWidth * 4) + "vw");
    $( "#songdesc, #fullscreenbtn, #seqinfo" ).css("font-size", (screenWidth * 1.5) + "vw");
    $( "#writerinfo" ).css("font-size", (screenWidth * 1.0) + "vw");
  }).resize();

  // Bind the swiperightHandler callback function to the swipe event on div.box
  $("#fullscreensheet" ).on( "swiped-left", swipeleftHandler );
  // Bind the swiperightHandler callback function to the swipe event on div.box
  $("#fullscreensheet" ).on( "swiped-right", swiperightHandler );
  // Callback function references the event target and adds the 'swiperight' class to it
  function swipeleftHandler( event ){
    slideControl(1);
  }
  // Callback function references the event target and adds the 'swiperight' class to it
  function swiperightHandler( event ){
    slideControl(-1);
  }

});
</script>
