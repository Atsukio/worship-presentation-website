<?php
require_once 'mysqli_connect.php';

if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_SONGSLIMIT,$validRoles)) {
	if(strtolower(pathinfo($_FILES["fileToUpload"]["name"], PATHINFO_EXTENSION)) == "xml") {
		$xml=simplexml_load_file($_FILES["fileToUpload"]["tmp_name"]) or die("Error: Cannot create object");
		echo("======= Reading from <b>" . $_FILES["fileToUpload"]["name"] . "
		=======</b><br />");
		echo("<br />");
		$i = 0;
		$sql = 'SELECT count(title) AS count FROM ' . TB_SONGS . ' WHERE title = ?';
		$pscheck = $mysqli->prepare($sql);
		$pscheck->bind_param("s", $title);
		date_default_timezone_set('Asia/Singapore');
		$sql = 'INSERT INTO ' . TB_SONGS . ' (title, writer, song_no, song_key, lyrics, sequence, date_modified, created_by)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
		$psins =  $mysqli->prepare($sql);
		$psins->bind_param("ssissssi",
		$title, $writer, $songno, $songkey,
		$lyrics, $seq, $date, $validId);
		foreach ($xml->children() as $songs) {
			$title = $songs->Title1;
			$writer = $songs->Writer;
			$songno = $songs->SongNumber;
			$songkey = $songs->MusicKey;
			$lyrics = $songs->Contents;
			$seq = $songs->Sequence;
			echo $i++ . ". ";
			$date = date("Y-m-d G:i:s");
			if(!$pscheck->execute()) {
				die("Error: Song check failed");
			}
			$row = $pscheck->get_result()->fetch_assoc();
			if($row['count'] > 0) {
				echo(htmlspecialchars($title) . ': <b style="color:#FF2D00;">Failed, already exists. </b><br>');
			} else {
				if(!$psins->execute()) {
					die("{$title}: Error: Song insert failed");
				}
				echo(htmlspecialchars($title) . ': <b style="color:#00FF55;">Success</b> <br>');
			}
		}
		echo("<br /><b>=========================
		END OF FILE
		=========================</b>");
	} else {
		die("Invalid file type");
	}
}
?>
