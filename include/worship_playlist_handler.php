<?php
require_once 'mysqli_connect.php';

if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_BROADCAST,$validRoles)) {
	if (isset($_POST['input']) && !empty($_POST['input'])) {
		switch($_POST['input']) {
			case "add":
			$sql = 'SELECT COUNT(id) AS count FROM ' . TB_PLAYLIST;
			if(!$result = $mysqli->query($sql)) {
				die("Error: Error getting sequence number");
			}
			$row = $result->fetch_assoc();
			$seqno = $row['count'];
			$sql = 'SELECT id, count(id) AS count FROM ' . TB_SONGS . ' WHERE id = ?';
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("i", $_POST['id']);
			if(!$ps->execute()) {
				die("Error: Song retrieve failed");
			}
			$row = $ps->get_result()->fetch_assoc();
			if($row['count'] === 0) {
				die("Error: Invalid song selected");
			}
			$sql = 'INSERT INTO ' . TB_PLAYLIST . '(song_id, seq_no)' .
			' VALUES (?, ?)';
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("ii", $row['id'], $seqno);
			if(!$ps->execute()) {
				die("Error: Song failed to be added");
			}
			die('<b>Playlist Status: </b><b class="w3-text-green">Successfully added to playlist</b>');
			break;

			case "empty":
			$sql = 'TRUNCATE TABLE ' . TB_PLAYLIST;
			if(!$mysqli->query($sql)) {
				die('Error: Failed to empty playlist');
			}
			die('<b>Playlist Status: </b><b class="w3-text-green">Successfully emptied playlist</b>');
			break;

			case "up":
			$sql = 'SELECT seq_no FROM ' . TB_PLAYLIST . ' WHERE id = ?';
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("i", $_POST['id']);
			if(!$ps->execute()) {
				die("Error: Failed to retrieve sequence number");
			}
			$row = $ps->get_result()->fetch_assoc();
			$seqno = $row['seq_no'];
			if (($seqno - 1) >= 0) {
				$sql = array(
				'UPDATE ' . TB_PLAYLIST . ' SET seq_no = -1 WHERE seq_no = ' . ($seqno - 1),
				'UPDATE ' . TB_PLAYLIST . ' SET seq_no = seq_no - 1 WHERE seq_no = ' . $seqno,
				'UPDATE ' . TB_PLAYLIST . ' SET seq_no = ' . $seqno . ' WHERE seq_no = -1'
				);
				foreach($sql as $value) {
					if(!$mysqli->query($value)) {
						die("Error: Sequence occured a problem while moving");
					}
				}
				die('<b>Playlist Status: </b><b class="w3-text-green">Song has been moved up</b>');
			} else {
				die('<b>Playlist Status: </b><b class="w3-text-red">Song is already at the top</b>');
			}
			break;

			case "down":
			$sql = 'SELECT COUNT(id) AS count FROM ' . TB_PLAYLIST;
			if(!$result = $mysqli->query($sql)) {
				die("Error: Failed to retrieve total sequence");
			}
			$row = $result->fetch_assoc();
			$count = $row['count'];
			$sql = 'SELECT seq_no FROM ' . TB_PLAYLIST . ' WHERE id = ?';
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("i", $_POST['id']);
			if(!$ps->execute()) {
				die("Error: Failed to retrieve sequence number");
			}
			$row = $ps->get_result()->fetch_assoc();
			$seqno = $row['seq_no'];
			if (($seqno + 1) < $count) {
				$sql = array(
				'UPDATE ' . TB_PLAYLIST . ' SET seq_no = -1 WHERE seq_no = ' . ($seqno + 1),
				'UPDATE ' . TB_PLAYLIST . ' SET seq_no = seq_no + 1 WHERE seq_no = ' . $seqno,
				'UPDATE ' . TB_PLAYLIST . ' SET seq_no = ' . $seqno . ' WHERE seq_no = -1'
				);
				foreach($sql as $value) {
					if(!$mysqli->query($value)) {
						die("Error: Sequence occured a problem while moving");
					}
				}
				die('<b>Playlist Status: </b><b class="w3-text-green">Song has been moved down</b>');
			} else {
				die('<b>Playlist Status: </b><b class="w3-text-red">Song is already at the bottom</b>');
			}
			break;

			case "delete":
			$sql = 'SELECT seq_no FROM ' . TB_PLAYLIST . ' WHERE id = ?';
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("i", $_POST['id']);
			if(!$ps->execute()) {
				die("Error: Failed to retrieve sequence number");
			}
			$row = $ps->get_result()->fetch_assoc();
			$seqno = $row['seq_no'];
			$sql = 'DELETE FROM ' . TB_PLAYLIST . ' WHERE id = ?';
			$ps = $mysqli->prepare($sql);
			$ps->bind_param("i", $_POST['id']);
			if(!$ps->execute()) {
				die("Error: Song failed to be deleted");
			}
			$sql = 'UPDATE ' . TB_PLAYLIST .
			' SET seq_no = seq_no - 1
			WHERE seq_no > ' . $seqno;
			if(!$mysqli->query($sql)) {
				die("ERROR: Failed to move song sequences");
			}
			die('<b>Playlist Status: </b><b class="w3-text-green">Successfully deleted song from playlist</b>');
			break;
		}
	}
}
?>
