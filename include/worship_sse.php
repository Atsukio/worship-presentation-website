<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

require_once 'mysqli_connect.php';

$playlistControl = $playlist = $worshipStatus = $worshipInfo = '';
$sql = 'SELECT COUNT(id) AS count FROM ' . TB_PLAYLIST;
if(!$result = $mysqli->query($sql)) {
	die("Error: Failed to retrieve playlist count");
}
$row = $result->fetch_assoc();
$count = $row['count'];
$sql = 'SELECT p.id AS id, p.seq_no AS seq_no, s.title AS title, s.id AS song_id
FROM ' . TB_PLAYLIST . ' p, ' . TB_SONGS . ' s ' .
' WHERE p.seq_no = ?' .
' AND p.song_id = s.id';
$ps = $mysqli->prepare($sql);
$ps->bind_param("i", $seqno);
for ($i = 0; $i < $count; $i++) {
	$seqno = $i;
	if(!$ps->execute()) {
		die("Error: Song sequence search failed");
	}
	$row = $ps->get_result()->fetch_assoc();
	$playlistControl .= '<li name="' . htmlspecialchars($row['id']) .
	'" value="' . htmlspecialchars($row['song_id']) .
	'" style="overflow: hidden;">' . htmlspecialchars(($row['seq_no'] + 1)) .
	'. ' . htmlspecialchars($row['title']) .
	'<span class="w3-right">' .
	'<button type="button" class="w3-border w3-card w3-gray w3-round-small playlist-move-up w3-margin-left" ' .
	'style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button>' .
	'<button type="button" class="w3-border w3-card w3-gray w3-round-small playlist-move-down w3-margin-left" ' .
	'style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button>' .
	'<button type="button" class="w3-border w3-card w3-gray w3-round-small playlist-delete w3-margin-left" ' .
	'style="cursor:pointer;"><i class="fas fa-times"></i></button></span></li>';

	$playlist .= '<li name="' . htmlspecialchars($row['id']) .
	'" value="' . htmlspecialchars($row['song_id']) . '" style="overflow: hidden;">' .
	htmlspecialchars($row['title']) . '</li>';
}

$sql = 'SELECT u.username AS username, u.id AS id, COUNT(u.id) AS count,
l.current_song AS song, l.current_sequence AS sequence
FROM ' . TB_LIVE . ' l, ' . TB_USERS . ' u '.
' WHERE l.userID = u.id
LIMIT 1';
if(!$result = $mysqli->query($sql)) {
	die("Error: Failed to retrieve results");
}
$row = $result->fetch_assoc();
if($row['count'] > 0) {
	$username = $row['username'];
	$userID = $row['id'];
	$song = $row['song'];
	$sequence = $row['sequence'];
	$worshipInfo = '<div class="w3-dropdown-hover"><button class="w3-border w3-red w3-round w3-padding w3-center" ' .
	'style="font-size:16px;font-weight:bold;padding-left:24px;padding-right:24px;">' .
	'<i class="fas fa-broadcast-tower w3-text-black w3-margin-left w3-margin-right"></i><span class="w3-mobile">ON-AIR</span><span class="w3-margin-right w3-hide-small"></span></button>' .
	'<div class="w3-dropdown-content w3-bar-block w3-border">' .
	'<label class="w3-bar-item w3-red">Broadcasting By <b><u>' . $username . '</u></b></label></div></div>';
	$worshipStatus = "ON-AIR";
} else {
	$username = '';
	$userID = '';
	$song = '-1';
	$sequence = '-1';
	$worshipStatus = "OFF-AIR";
	$worshipInfo = '<button class="w3-border w3-gray w3-round w3-padding w3-center" ' .
	'style="font-size:16px;font-weight:bold;padding-left:24px;padding-right:24px;">' .
	'<i class="fas fa-power-off w3-margin-left w3-margin-right"></i><span class="w3-mobile">OFF-AIR</span><span class="w3-margin-right w3-hide-small"></span></button>';
}

echo "retry: 500\n";
echo "event: playlistControl\n";
echo "data: {$playlistControl}\n\n";

echo "retry: 500\n";
echo "event: playlist\n";
echo "data: {$playlist}\n\n";

echo "retry: 500\n";
echo "event: broadcast\n";
echo 'data: {"status": "' . $worshipStatus . '", "id": "' . $userID . '", "username": "' . $username . '", "song": "' . $song . '", "sequence": "' . $sequence . '"}';
echo "\n\n";

echo "retry: 500\n";
echo "event: worshipStatus\n";
echo "data: {$worshipInfo}\n\n";

flush();
?>
