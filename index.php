<?php require_once 'include/head.php'; ?>
<script>
if (window.location.href.includes("<?php echo $const['LOCATION']; ?>index") == false) {
	window.location.replace("<?php echo $const['LOCATION']; ?>index");
}

<?php
if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
echo <<<HEREDOC
function textAreaAdjust(o) {
  o.style.height = "1px";
  o.style.height = (25+o.scrollHeight)+"px";
}


<!-- When document loads finish START -->
$(document).ready(function(){
	// On form submit
	$('#description-editor').submit(function(event){
		// Cancel submit event
		event.preventDefault();
		var datastring = $("#description-editor").serialize();
		var xmlhttp = new XMLHttpRequest;
		document.getElementById('description-editor-submit').disabled = 'true';
		document.getElementById('description-editor-submit').innerHTML = '<b><i class="fa fa-spinner fa-spin"></i></b>';
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if (this.responseText.includes('UPDATE failed: ')) {
					alert(this.responseText);
				} else {
					var data = JSON.parse(this.responseText);
					document.getElementById('description-title').innerHTML = data.title;
					document.getElementById('description-subtitle').innerHTML = data.subtitle;
					document.getElementById('description-desc').innerHTML = data.desc;
					document.getElementById('description-editor-submit').innerHTML = '<i class="fas fa-save"></i> Save</i>';
					document.getElementById('description-editor-submit').disabled = '';
					document.getElementById('description-editor').style.display = 'none';
					document.getElementById('description').style.display = '';
				}
			}
		};
		xmlhttp.open("POST", "{$const['LOCATION']}include/index_handler.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("type=desc&" + datastring);
	});
});
<!-- When document loads finish END -->
HEREDOC;
}
?>
</script>
</head>
<body
<?php
if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
echo <<<HEREDOC
	onload="textAreaAdjust(document.getElementById('description-editor-desc'));"
HEREDOC;
}
?>
class="w3-animate-opacity">
<?php
require_once 'include/header.php';
require_once 'include/carousel.php';

$sql = "SELECT name, value FROM " . TB_DESC;
if(!$result = $mysqli->query($sql)) {
	die("Error: " . $mysqli->error);
}
while($row = $result->fetch_assoc()) {
	if ($row['name'] == "desc_title") {
		$desc_title = htmlspecialchars($row['value']);
	} else if ($row['name'] == "desc_subTitle") {
		$desc_subTitle = htmlspecialchars($row['value']);
	} else if ($row['name'] == "desc_desc") {
		$desc_desc = htmlspecialchars($row['value']);
	}
}
?>
<!-- Description -->
<div id="description" class="w3-container w3-center w3-content" style="max-width:600px;">
  <h3 id="description-title" class="w3-wide w3-text-blue"><?php echo $desc_title; ?></h3>
  <p id="description-subtitle" class="w3-opacity"><i><?php echo $desc_subTitle; ?></i></p>
  <p id="description-desc" class="w3-justify"><?php echo $desc_desc; ?></p>
	<?php
	$result = $mysqli->query("SELECT u.username AS user
	FROM users u, description d
	WHERE d.name = 'desc_desc' AND d.userID = u.id");
	$row = $result->fetch_assoc();
	$desc_writer = $row['user'];
	?>
	<p class="w3-text-gray w3-left-align"> - <b><a title="View <?php echo $desc_writer; ?>'s profile" href="profile/<?php echo $desc_writer; ?>" id="description-user"><?php echo $desc_writer; ?></a></b></p>
	<?php
if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
echo <<<HEREDOC
<button type="button" class="w3-btn w3-light-gray w3-border w3-round-small" onclick="document.getElementById('description').style.display = 'none'; document.getElementById('description-editor').style.display = ''; textAreaAdjust(document.getElementById('description-editor-desc'));"><i class="fas fa-edit"> Edit</i></button>
HEREDOC;
}
?>
</div>
<?php
if(array_intersect($RL_FULL,$validRoles) || array_intersect($RL_INDEX,$validRoles)) {
echo <<<HEREDOC
<form id="description-editor" class="w3-container w3-center w3-content" style="display:none; max-width:600px;">
  <p><input style="font-size:1.3em;" id="description-editor-title" type="text" class="w3-wide w3-text-blue w3-center w3-input w3-border" name="title" value="$desc_title"></p>
  <p id="description-editor-subtitle" class="w3-opacity"><i><input class="w3-center w3-input w3-border" type="text" name="subtitle" value="$desc_subTitle"></i></p>
  <p class="w3-justify"><textarea id="description-editor-desc" class="w3-input w3-border" onkeyup="textAreaAdjust(this)" style="width:100%; resize:none; overflow:hidden;" name="desc">$desc_desc</textarea></p>
	<button type="button" class="w3-btn w3-red w3-border" onclick="document.getElementById('description').style.display = ''; document.getElementById('description-editor').style.display = 'none';"><i class="fas fa-times"></i> Close</button>
	<button id="description-editor-submit" type="submit" class="w3-btn w3-green w3-border"><i class="fas fa-save"></i> Save</button>
</form>
HEREDOC;
}
?>
<br>
<?php require_once 'include/footer.php'; ?>
</body>
</html>
