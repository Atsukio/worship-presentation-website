<?php
require_once 'include/mysqli_connect.php';
$cookie_name="uName";

$passcheck=$usercheck='incorrect';
$usernameErr=$passwordErr="";
$username=$password="";

if(!$validToken) {
	if($_SERVER["REQUEST_METHOD"]=="POST"){
		//Checking for errors in the username ||START||
		if (empty($_POST["username"])){
			$usernameErr = "Username is required";
		}else{
			$username = $_POST["username"];
			if (!preg_match("/^[a-zA-Z0-9_]*$/",$username)){
				$usernameErr = "uName_invalid";
				echo "Only alphanumeric symbols and underscore allowed";
				//Checking for errors in the username ||END||
			}else{
				//Checking for the username in the database||START||
				if(isset($_POST["username"]) && !empty($_POST["username"])) {
					$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
					$username = $_POST['username'];
					$password = $_POST['password'];
					$hash = password_hash($password, PASSWORD_DEFAULT);

					//Prepre SQL statement
					$sql = "SELECT password, token, COUNT(id) AS COUNT FROM " . TB_USERS .
					" WHERE username = ?";
					$stmt = mysqli_prepare($mysqli, $sql);
					mysqli_stmt_bind_param($stmt, "s", $username);
					mysqli_stmt_execute($stmt);
					$result = $stmt->get_result();
					$row = $result->fetch_assoc();
					//$row = $mysqli->query($sql)->fetch_assoc();
					if(password_verify($password, $row['password'])){
						if ($row['COUNT'] > 0) {
							if ($row['token'] == '') {

								//Checking the database in case the token generated matches
								$sql = "SELECT COUNT(id) AS count FROM " .TB_USERS. "
								WHERE token = ?";
								$stmt = mysqli_prepare($mysqli, $sql);
								mysqli_stmt_bind_param($stmt, "s", $token);
								do{
									$token = openssl_random_pseudo_bytes(16);
									$token = bin2hex($token);
									mysqli_stmt_execute($stmt);
									$result = $stmt->get_result();
									$row = $result->fetch_assoc();
								}while($row['count'] > 0);

								mysqli_stmt_close($stmt);

								$sql = "UPDATE users
								SET token = ?
								WHERE username = ?";
								//Prepare SQL statement
								$stmt = mysqli_prepare($mysqli , $sql);
								mysqli_stmt_bind_param($stmt , "ss" , $token, $username);
								mysqli_stmt_execute($stmt);
								setcookie(CK_TOKEN,$token,time()+(86400*365),"/");
								$user = $_POST["username"];
							} else{
								$sql = "SELECT token FROM " .TB_USERS. "
								WHERE username = ?";
								$stmt = mysqli_prepare($mysqli, $sql);
								mysqli_stmt_bind_param($stmt, "s", $username);
								mysqli_stmt_execute($stmt);
								$result = $stmt->get_result();
								$row = $result->fetch_assoc();
								setcookie(CK_TOKEN,$row['token'],time()+(86400*365),"/");
								$user = $_POST["username"];
							}
							//Close prepared statement
						}
					}else{
						echo "Invalid username/password";
					}
				}
			}
		}
	}
}
//header('Location: index.php');
?>
