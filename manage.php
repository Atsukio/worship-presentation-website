<?php
require_once 'include/head.php';
if(!array_intersect($RL_FULL,$validRoles) || !array_intersect($RL_MANAGE,$validRoles)) {
echo("
</head>
<body class='w3-animate-opacity'>"
);
require_once 'include/header.php';
echo <<<HEREDOC
  <div class="w3-padding-large" style="min-height:80vh">
    <h1 class="w3-text-red w3-center w3-padding-large">No permission to view this page</h1>
    <h3 class="w3-center w3-padding-large">You must be logged in to view this page.</h3>
  </div>
HEREDOC;
require_once 'include/footer.php';
echo("</body>");
die();
}
?>

<script>
<!-- Open settings tab START -->
function openTab(tabName, elmnt) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("settingsTab");
  for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("settingsLink");
  for (i = 0; i < tablinks.length; i++) {
    if (tablinks[i].classList.contains("w3-text-blue")) {
      tablinks[i].classList.remove("w3-text-blue");
      tablinks[i].classList.add("w3-text-light-blue");
    }
  }
  document.getElementById(tabName).style.display = "initial";
  elmnt.classList.remove("w3-text-light-blue");
  elmnt.classList.add("w3-text-blue");
  if(tabName === "registrationTab") {
    $("#registerForm").trigger("reset");
    $("#usernameStatus").html("");
    $("#registerStatus").html("");
  }
}
<!-- Open settings tab END -->

function searchByUsername(value) {
  var xmlhttp = new XMLHttpRequest;
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      document.getElementById('memberList').innerHTML = this.responseText;
    }
  };
  xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/profile_handler.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("username=" + value);
}

function showProfile(user) {
  var xmlhttp = new XMLHttpRequest;
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      var res = JSON.parse(this.responseText);
      if(res.status == 'success') {
        var d = new Date();
        var tempRoles = res.roles.split(",");
        $("#updateRolesForm").trigger("reset");
        $("#profileModal").show();
        $(document.body).css("overflow", "hidden");
        $("#profileModal_avatar").attr("src", res.avatar+"?"+d.getTime()).fadeIn();
        $("#profileModal_avatar").attr("alt", res.avatarAlt).fadeIn();
        $("#profileModal_username").text(res.username);
        if(res.nickname) {
          $("#profileModal_nickname").text("(" + res.nickname + ")");
        } else {
          $("#profileModal_nickname").text("");
        }
        $("#profileModal_role").text(res.roles);
        $.each(tempRoles, function(i, roles) {
          $("#updateRolesForm :checkbox[value=" + roles + "]").prop("checked","true");
        });
        $("#profileModal_date").text(res.date);
        $("#profileModal_email").text(res.email);
      } else {
        alert(res.status);
      }
    }
  };
  xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/profile_handler.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("profile=" + user);
}

function deleteUser() {
  if (confirm("Are you sure you want to DELETE this account?")) {
    var xmlhttp = new XMLHttpRequest;
    xmlhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        if(this.responseText == "success") {
          $("#profileModal").css("display","none");
          $("body").css("overflow","auto");
          searchByUsername("");
        } else {
          $("#updateStatus").html(this.responseText);
        }
      }
    };
    xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/profile_handler.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xmlhttp.send("type=deleteUser&username=" + $("#profileModal_username").text());
  }
}

function usernameCheck(value) {
  var xmlhttp = new XMLHttpRequest;
  $('#usernameStatus').html('<b><i class="fa fa-spinner fa-spin"></i></b>')
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      $('#usernameStatus').html(this.responseText);
    }
  };
  xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/profile_handler.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("type=usernameCheck&username=" + value);
}

$(document).ready(function(){

  var profileModal = document.getElementById('profileModal');

  window.onclick = function(event) {
   if (event.target == profileModal) {
     document.body.style.overflow = "auto";
     profileModal.style.display = "none";
   }
  }

  $( "form" ).submit(function( event ) {
    if($(this).attr("id") == "registerForm" || $(this).attr("id") == "updateRolesForm") {
      event.preventDefault();
      var form = $(this).attr("id");
      var datastring = "type=" + $(this).attr("id");
      if(form == "updateRolesForm") {
        datastring += "&username=" + $("#profileModal_username").text();
      }
      var x = $( this ).serializeArray();
      let roles = false;
      $.each(x, function(i, field){
        if(field.name == "roles") {
          if(roles) {
            datastring += "," + field.value;
          } else {
            if(datastring) {
              datastring += "&roles=" + field.value;
              roles = true;
            }
          }
        } else {
          if(datastring) {
            datastring += "&";
          }
          datastring += field.name + "=" + field.value;
        }
      });
      xmlhttp = new XMLHttpRequest;
      xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
          if(form == "registerForm") {
            if(this.responseText.includes("successfully")) {
              $('#usernameStatus').html("");
            }
            $("#registerStatus").html(this.responseText);
          } else if(form == "updateRolesForm") {
            $("#updateStatus").html(this.responseText);
          }
          if(this.responseText.includes("successfully")) {
            if($( "#profileModal" ).is(":visible")) {
              showProfile($("#profileModal_username").text());
            }
          }
          searchByUsername("");
        }
      };
      xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/profile_handler.php", true);
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlhttp.send(datastring);
    }
  });
});
</script>

<?php
echo <<<HEREDOC
</head>
<body class='w3-animate-opacity' onload='searchByUsername("");'>"
HEREDOC;
?>
<?php require_once 'include/header.php'; ?>
<div id="profileModal" class="w3-modal" style="position:fixed;">
  <div class="w3-modal-content w3-animate-opacity w3-card-4 w3-display-middle w3-mobile" style="margin-top:48px;margin-left:0;max-width:90vw;">
    <header class="w3-container w3-teal">
      <span onclick="document.getElementById('profileModal').style.display='none';document.body.style.overflow='auto';"
      class="w3-button w3-display-topright">&times;</span>
      <h2>Manage User</h2>
    </header>
    <div style="max-height:50vh;overflow:auto;word-wrap:break-word;">
      <div class="w3-container w3-section">
        <img id="profileModal_avatar"class="w3-image w3-container w3-border w3-round w3-margin-left w3-col s3" style="padding:8px;" />
        <div class="w3-col s7" style="padding-left:24px;">
          <span class="w3-text-blue w3-large" style="word-wrap:break-word;"><b id="profileModal_username"></b></span><br />
          <span class="w3-text-gray w3-small"><b id="profileModal_nickname"></b></span><br />
          <span class="w3-text-black w3-small">Roles: <b id="profileModal_role"></b></span><br />
          <span class="w3-text-black w3-small">Joined date: <b id="profileModal_date"></b></span><br />
          <span class="w3-text-black w3-small">Email address: <b id="profileModal_email"></b></span><br />
        </div>
      </div>
      <div class="w3-panel">
        <form id="updateRolesForm">
          <label class="w3-text-teal">Change password</label>
          <input class="w3-input w3-border" type="password" name="password" />
          <br />
          <?php require_once 'include/roles.php'; ?>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_ADMIN']; ?>" /> <?php echo $const['R_ADMIN']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_ASSIST']; ?>" /> <?php echo $const['R_ASSIST']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_BROADCAST']; ?>" /> <?php echo $const['R_BROADCAST']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_EDITOR']; ?>" /> <?php echo $const['R_EDITOR']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_MEMBER']; ?>" /> <?php echo $const['R_MEMBER']; ?></span>
          <span class="w3-left w3-col" id="updateStatus"></span>
          <div class="w3-col">
            <button type="submit" class="w3-btn w3-right w3-card w3-green w3-section w3-margin-left">Save</button>
            <button type="button" class="w3-btn w3-right w3-card w3-red w3-section" onclick="deleteUser();">Delete Account</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

  <div class="w3-container" style="min-height:70vh;">
    <div class="w3-col m3 w3-margin-top">
      <h2 class="w3-center w3-text-gray">Settings</h2>
      <div class="w3-bar-block w3-center">
        <button class="w3-bar-item w3-button w3-text-blue w3-hover-text-blue settingsLink" onclick="openTab('membersTab', this)">Members</button>
        <button class="w3-bar-item w3-button w3-text-light-blue w3-hover-text-blue settingsLink" onclick="openTab('registrationTab', this)">Registration</button>
      </div>
    </div>
    <div class="w3-col m9 w3-margin-top w3-padding">
      <div id="membersTab" class="settingsTab">
        <span class="w3-text-teal">Search for a user by username</span><br />
        <input type="text" id="searchByUsername" class="w3-input w3-border"  onkeyup="searchByUsername(value);" placeholder="Enter username here..."/>
        <div id="memberList" class="w3-section w3-border" style="overflow:auto;min-height:192px;height:auto;max-height:75vh;padding:16px;padding-bottom:32px;">
        </div>
      </div>
      <div id="registrationTab" class="settingsTab" style="display:none;">
        <h2 class="w3-text-blue">Registration</h2>
        <form id="registerForm">
          <label class="w3-text-teal">Username: </label><span id="usernameStatus"></span>
          <input type="text" class="w3-input" onkeyup="usernameCheck(this.value);" name="username" />
          <label class="w3-text-teal">Password:</label>
          <input type="password" class="w3-input" name="password" /><br />
          <label class="w3-text-teal"><b><u>Roles:</u></b></label><br />
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_ADMIN']; ?>" /> <?php echo $const['R_ADMIN']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_ASSIST']; ?>" /> <?php echo $const['R_ASSIST']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_BROADCAST']; ?>" /> <?php echo $const['R_BROADCAST']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_EDITOR']; ?>" /> <?php echo $const['R_EDITOR']; ?></span>
          <span class="w3-col l3 m4 s6"><input class="w3-check" type="checkbox" name="roles" value="<?php echo $const['R_MEMBER']; ?>" /> <?php echo $const['R_MEMBER']; ?></span>
          <span class="w3-left w3-col" id="registerStatus"></span>
          <div class="w3-col w3-margin-top">
            <button type="submit" class="w3-btn w3-right w3-card w3-green w3-section">Register</button>
            <button type="reset" class="w3-btn w3-right w3-card w3-red w3-section w3-margin-right" onclick='$("#usernameStatus").html("");$("#registerStatus").html("");'>Reset</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php require_once 'include/footer.php'; ?>
</body>
