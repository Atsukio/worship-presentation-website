<?php require_once 'include/head.php'; ?>
<?php require_once 'include/song_sheet_scripts.php'; ?>
<script>
<!-- Slide control START -->
<?php
$songID = 'song.id';
if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
	$songID = 'editID';
	echo 'var editID = -1';
}
?>

function slideControl(num) {
	document.getElementById('sheetlyrics').innerHTML = '';
	document.getElementById('seqinfo').innerHTML = '';
	var item = "", tempLyrics = "", temp = "";
	if (sheet.sequence == '') {
		tempLyrics = sheet.lyrics;
  } else {
		if (sheet.currentSequence + num >= 0 && sheet.currentSequence + num < sheet.sequence.length) {
			sheet.currentSequence += num;
		}

		document.getElementById("prevslidebtn").style.display = "none";
		document.getElementById("nextslidebtn").style.display = "none";

		if (!document.getElementById('scrollmode').checked) {
      tempLyrics = sheet.lyrics[sheet.currentSequence];
			if (sheet.currentSequence != 0) {
				document.getElementById("prevslidebtn").style.display = "";
			}
			if (sheet.currentSequence != sheet.sequence.length - 1) {
				document.getElementById("nextslidebtn").style.display = "";
			}
			document.getElementById("seqinfo").innerHTML = '';
			for (var i = 0; i < sheet.sequence.length; i++) {
				if (sheet.currentSequence == i) {
					document.getElementById("seqinfo").innerHTML += "<span class='w3-badge w3-green' style='display:inline-block;'>" + sheet.sequence[sheet.currentSequence] + "</span> ";
				} else {
					document.getElementById("seqinfo").innerHTML += "<span>" + sheet.sequence[i] + "</span>" + " ";
				}
			}
		} else {
			tempLyrics = '';
			for (var i = 0; i < sheet.lyrics.length; i++) {
				tempLyrics += sheet.lyrics[i] + "<br><br>";
			}
			for (var i = 0; i < sheet.sequence.length; i++) {
					document.getElementById("seqinfo").innerHTML += " " + sheet.sequence[i];
			}
		}
	}

	if (document.getElementById("showchords").checked) {
			if (song.key != "") {
				if (song.transpose != "") {
					document.getElementById("songdesc").innerHTML = song.title +
					"<span class='w3-text-gray'> [KEY: " + song.transpose + "]</span>";

				} else {
					document.getElementById("songdesc").innerHTML = song.title +
					"<span class='w3-text-gray'> [KEY: " + song.key + "]</span>";
				}
				tempLyrics = tempLyrics.replace(/display:none;/g, "");
			}
	} else {
		document.getElementById("songdesc").innerHTML = song.title;
	}
	document.getElementById("sheetlyrics").innerHTML = tempLyrics;
}
<!-- Slide control END -->

<!-- Select song START -->
function selectSong(event) {
	var targ = event.target || event.srcElement;
<?php
	if(array_intersect($RL_SONGSFULL,$validRoles)) {
		echo <<<HEREDOC
	  songClear();
HEREDOC;
	}
?>
	// Start of Music Sheet
	document.getElementById('sheetlyrics').innerHTML = '';
	document.getElementById('songdesc').innerHTML = '';
	document.getElementById('seqinfo').innerHTML = '';
	// End of Music Sheet
  <?php
  if(array_intersect($RL_SONGSFULL,$validRoles)) {
    echo <<<HEREDOC
    document.getElementById('verses').innerHTML = "";
  	document.getElementById('sequence').innerHTML = "";
  	document.getElementById("musiceditorform").reset();
  	if (targ.value == "-1") {
  		song.id = -1;
			window.history.replaceState("", "", "{$const['LOCATION']}music/");
  		document.getElementById("currentsong").innerHTML = "Creating a new song";
  		document.getElementById("sm_selectedsong").innerHTML = "None";
  		document.getElementById("deletesong").style.display = "none";
  		document.getElementById("savesubmit").innerHTML = "<i class='fas fa-file-export'></i> Submit";
  	} else {
  		retrieveSong(targ.value);
  	}
HEREDOC;
  } else {
    echo <<<HEREDOC
    retrieveSong(targ.value);
HEREDOC;
  }
  ?>
}
<!-- Select song END -->

<!-- Retrieve song START -->
function retrieveSong(id) {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = this.responseText.split("~split~");
			// Start of Music Sheet
			sheet.currentSequence = 0;
			song.id = id;
			song.title = result[0];
			song.writer = result[1];
			song.no = result[2];
			song.key = result[3];
			song.transpose = result[4];
			song.lyrics = result[5];
      sheet.sequence = result[6].split(",");
			sheet.lyrics = song.lyrics.replace(/\n/g, "<br>");
			document.getElementById("songdesc").innerHTML = song.title;
			$("#writerinfo").html(song.writer);
      sheetLyrics();
      slideControl(0);
			// End of Music Sheet
      <?php
      if(array_intersect($RL_SONGSFULL,$validRoles)) {
        echo <<<HEREDOC
        document.getElementById('sequence').innerHTML = '';
        document.getElementById("currentsong").innerHTML = song.title;
  			document.getElementById("sm_selectedsong").innerHTML = song.title;
  			document.getElementById("musiceditorform").elements.namedItem("title").value = decodehsc(song.title);
  			document.getElementById("musiceditorform").elements.namedItem("writer").value = decodehsc(song.writer);
  			document.getElementById("musiceditorform").elements.namedItem("songno").value = decodehsc(song.no);
  			document.getElementById("musiceditorform").elements.namedItem("songkey").value = decodehsc(song.key);
  			document.getElementById("musiceditorform").elements.namedItem("transpose").value = decodehsc(song.transpose);
  			document.getElementById("musiceditorform").elements.namedItem("lyrics").value = decodehsc(song.lyrics);
  			for (var i = 0; i < sheet.sequence.length; i++) {
  				loadSequence(sheet.sequence[i]);
  			}
  			versesCheck();
  			document.getElementById("deletesong").style.display = "initial";
  			document.getElementById("savesubmit").innerHTML = "<i class='fas fa-save'></i> Save";
HEREDOC;
      }
      ?>
			if(song.id != -1) {
				window.history.replaceState("", "", "<?php echo $const['LOCATION']; ?>music/" + song.id);
			} else {
				window.history.replaceState("", "", "<?php echo $const['LOCATION']; ?>music/");
			}
		}
	};
	xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/song_handler.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("input=select&id=" + id);
}
<!-- Retrieve song END -->

<!-- Song search START -->
function songSearch() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("songinput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("songlist");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
	<?php
	if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
		echo <<<HEREDOC
		ul = document.getElementById("songlistMember");
    li = ul.getElementsByTagName("li");
		for (i = 0; i < li.length; i++) {
        if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
HEREDOC;
	}
	?>
}
<!-- Song search END -->

<!-- Retrieve song list START -->
function retrieveSongList() {
	var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
<?php
if(array_intersect($RL_SONGSFULL,$validRoles)) {
echo <<<HEREDOC
        document.getElementById("songinput").value = "";
        document.getElementById("songlist").innerHTML = '<li value="-1">&lt Create New Song &gt</li>' + this.responseText;
        if (song.id != -1) {
          retrieveSong(song.id);
        } else if (document.getElementById("musiceditorform").elements.namedItem("title").value != "" &&
          document.getElementById("currentsong").innerHTML == "Creating a new song" &&
          song.id == -1) {
          var ul = document.getElementById("songlist");
          var li = ul.getElementsByTagName("li");
          for (i = 0; i < li.length; i++) {
            if (li[i].innerHTML == document.getElementById("musiceditorform").elements.namedItem("title").value) {
              document.getElementById("currentsong").innerHTML = li[i].innerHTML;
              song.id = li[i].value;
							retrieveSong(song.id);
              break;
            }
          }
        }
HEREDOC;
} else {
echo <<<HEREDOC
      document.getElementById("songlist").innerHTML = this.responseText;
HEREDOC;
}
?>
			}
		};
	xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/song_handler.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("input=retrievelist");
}
<!-- Retrieve song list END -->

<?php
if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
echo <<<HEREDOC
<!-- Member song list START -->
function memberSongList() {
	var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
        document.getElementById("songinput").value = "";
				$('#songlistMember').html('<li value="-1">&lt Create New Song &gt</li>' + this.responseText);
				if(getReq) {
					getReq = false;
				} else if ({$songID} != -1) {
          memberRetrieveSong({$songID});
        } else if (document.getElementById("musiceditorform").elements.namedItem("title").value != "" &&
          document.getElementById("currentsong").innerHTML == "Creating a new song" &&
          {$songID} == -1) {
          var ul = document.getElementById("songlistMember");
          var li = ul.getElementsByTagName("li");
          for (i = 0; i < li.length; i++) {
            if (li[i].innerHTML == document.getElementById("musiceditorform").elements.namedItem("title").value) {
              document.getElementById("currentsong").innerHTML = li[i].innerHTML;
              {$songID} = li[i].value;
							memberRetrieveSong({$songID});
              break;
            }
          }
        }
			}
		};
	xmlhttp.open("POST", "{$const['LOCATION']}include/song_handler.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("input=retrievelistMember");
}
<!-- Member song list END -->

<!-- Member Select song START -->
function memberSelectSong(event) {
	var targ = event.target || event.srcElement;
  songClear();
	// Start of Music Sheet
	document.getElementById('sheetlyrics').innerHTML = '';
	document.getElementById('songdesc').innerHTML = '';
	document.getElementById('seqinfo').innerHTML = '';
	// End of Music Sheet
  document.getElementById('verses').innerHTML = "";
	document.getElementById('sequence').innerHTML = "";
	document.getElementById("musiceditorform").reset();
	if (targ.value == "-1") {
		{$songID} = -1;

		document.getElementById("currentsong").innerHTML = "Creating a new song";
		window.history.replaceState("", "", "{$const['LOCATION']}music/");
		document.getElementById("deletesong").style.display = "none";
		document.getElementById("savesubmit").innerHTML = "<i class='fas fa-file-export'></i> Submit";
	} else {
		memberRetrieveSong(targ.value);
	}
}
<!-- Member Select song END -->

<!-- Member Retrieve song START -->
function memberRetrieveSong(id) {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = this.responseText.split("~split~");
			// Start of Music Sheet
			sheet.currentSequence = 0;
			{$songID} = id;
HEREDOC;
echo <<<'HEREDOC'
			song.title = result[0];
			song.writer = result[1];
			song.no = result[2];
			song.key = result[3];
			song.transpose = result[4];
			song.lyrics = result[5];
			sheet.sequence = result[6].split(",");
			sheet.lyrics = song.lyrics.replace(/\n/g, "<br>");
			document.getElementById("songdesc").innerHTML = song.title;
			$("#writerinfo").html(song.writer);
HEREDOC;
echo <<<HEREDOC
      sheetLyrics();
      slideControl(0);
			// End of Music Sheet
      document.getElementById('sequence').innerHTML = '';
      document.getElementById("currentsong").innerHTML = song.title;
			document.getElementById("musiceditorform").elements.namedItem("title").value = decodehsc(song.title);
			document.getElementById("musiceditorform").elements.namedItem("writer").value = decodehsc(song.writer);
			document.getElementById("musiceditorform").elements.namedItem("songno").value = decodehsc(song.no);
			document.getElementById("musiceditorform").elements.namedItem("songkey").value = decodehsc(song.key);
			document.getElementById("musiceditorform").elements.namedItem("transpose").value = decodehsc(song.transpose);
			document.getElementById("musiceditorform").elements.namedItem("lyrics").value = decodehsc(song.lyrics);
			for (var i = 0; i < sheet.sequence.length; i++) {
				loadSequence(sheet.sequence[i]);
			}
			versesCheck();
			document.getElementById("deletesong").style.display = "initial";
			document.getElementById("savesubmit").innerHTML = "<i class='fas fa-save'></i> Save";
			if({$songID} != -1) {
				window.history.replaceState("", "", "{$const['LOCATION']}music/" + {$songID});
			} else {
				window.history.replaceState("", "", "{$const['LOCATION']}music/");
			}
		}
	};
	xmlhttp.open("POST", "{$const['LOCATION']}include/song_handler.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("input=select&id=" + id);
}
<!-- Member Retrieve song END -->
HEREDOC;
}
?>

<?php
if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_SONGSLIMIT,$validRoles)) {
  echo <<<HEREDOC
  <!-- Load sequence START -->
  function loadSequence(seq) {
  	var item = "";
  	for (var i = 1; i <= 10; i++) {
  		if (seq == i) {
  			item = i;
  		}
  	}
  	if (seq == "p") {
  		item = "prechorus";
  	} else if (seq == "c") {
  		item = "chorus";
  	} else if (seq == "b") {
  		item = "bridge";
  	} else if (seq == "q") {
  		item = "prechorus 2";
  	} else if (seq == "t") {
  		item = "chorus 2";
  	} else if (seq == "w") {
  		item = "bridge 2";
  	} else if (seq == "e") {
  		item = "ending";
  	}
  	if (item != "") {
  		document.getElementById('sequence').innerHTML += '<li>' + item + '<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  	}
  }
  <!-- Load sequence END -->

  <!-- Song check START -->
  function songCheck(str) {
  	if ({$songID} == -1) {
  		songClear();
  		document.getElementById('songstatus').innerHTML = '<b>Song Title: <i class="fa fa-spinner fa-spin"></i></b>';
  		if (str.length == 0) {
  			songClear();
  		} else {
  			var xmlhttp = new XMLHttpRequest();
  			xmlhttp.onreadystatechange = function() {
  				if (this.readyState == 4 && this.status == 200) {
  					document.getElementById('songstatus').innerHTML = this.responseText;
  				}
  			};
  			xmlhttp.open("POST", "{$const['LOCATION']}include/song_handler.php", true);
  			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  			xmlhttp.send("input=check" + "&title=" + str);
  		}
  	}
  }
  <!-- Song check END -->

  <!-- Song clear START -->
  function songClear() {
  	document.getElementById('songstatus').innerHTML = '';
  	document.getElementById('savesubmitstatus').innerHTML = '';
HEREDOC;
		if(array_intersect($RL_SONGSFULL,$validRoles)) {
		echo <<<HEREDOC
  	document.getElementById("playliststatus").innerHTML = '';
HEREDOC;
		}
		echo <<<HEREDOC
  }
  <!-- Song clear END -->

  <!-- Song delete START -->
  function songDelete() {
  	if (confirm("Are you sure you want to DELETE this song?")) {
      document.getElementById('deletesong').disabled = 'true';
      document.getElementById('deletesong').innerHTML = '<b><i class="fa fa-spinner fa-spin"></i></b>';
      var xmlhttp = new XMLHttpRequest();
      document.getElementById('savesubmitstatus').innerHTML = '<b><i class="fa fa-spinner fa-spin"></i> Deleting</b>';
  		xmlhttp.onreadystatechange = function() {
  			if (this.readyState == 4 && this.status == 200) {
          if(this.responseText.includes('successfully')) {
            {$songID} = -1;
            retrieveSongList();
HEREDOC;
								if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
									echo "memberSongList();";
								}
echo <<<HEREDOC
						document.getElementById('sheetlyrics').innerHTML = '';
						document.getElementById('songdesc').innerHTML = '';
						document.getElementById('seqinfo').innerHTML = '';
            document.getElementById("musiceditorform").reset();
            document.getElementById('verses').innerHTML = '';
            document.getElementById('sequence').innerHTML = "";
            document.getElementById("currentsong").innerHTML = "Creating a new song";
HEREDOC;
								if(array_intersect($RL_SONGSFULL,$validRoles)) {
									echo "document.getElementById('sm_selectedsong').innerHTML = 'None';";
								}
echo <<<HEREDOC
            document.getElementById("deletesong").style.display = "none";
            document.getElementById('deletesong').innerHTML = '<i class="fas fa-trash-alt"></i> Delete';
            document.getElementById('deletesong').disabled = '';
            document.getElementById("savesubmit").innerHTML = "<i class='fas fa-file-export'></i> Submit";
            songClear();
          }
          document.getElementById('savesubmitstatus').innerHTML = this.responseText;
  			}
  		};
  		xmlhttp.open("POST", "{$const['LOCATION']}include/song_handler.php", true);
  		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  		xmlhttp.send("input=delete" + "&id=" + {$songID});
      }
  }
  <!-- Song delete END -->
HEREDOC;
echo <<<'HEREDOC'
  <!-- Text add START -->
  function textAdd(txt) {
  	var lyrics = document.getElementById("songlyrics");
  	var last = lyrics.value.replace(/\n/g, "<br>");
  	if (last.substring(last.length - 4, last.length) != "<br>") {
  		lyrics.value += "\n";
  	}
  	lyrics.value += "[" + txt + "]" + "\n";
  	versesCheck();
  }
  <!-- Text add END -->

  <!-- Verses check START -->
  function versesCheck() {
  	var lyrics = document.getElementById('songlyrics').value;
  	var verses = "";
  	for (var i = 1; i <= 10; i++) {
  		if (lyrics.includes('[' + i + ']')) {
  			verses += '<li>' + i + '</li>';
  		}
  	}
  	if (lyrics.includes('[prechorus]')) {
  		verses += '<li>prechorus</li>';
  	}
  	if (lyrics.includes('[prechorus 2]')) {
  		verses += '<li>prechorus 2</li>';
  	}
  	if (lyrics.includes('[chorus]')) {
  		verses += '<li>chorus</li>';
  	}
  	if (lyrics.includes('[chorus 2]')) {
  		verses += '<li>chorus 2</li>';
  	}
  	if (lyrics.includes('[bridge]')) {
  		verses += '<li>bridge</li>';
  	}
  	if (lyrics.includes('[bridge 2]')) {
  		verses += '<li>bridge 2</li>';
  	}
  	if (lyrics.includes('[ending]')) {
  		verses += '<li>ending</li>';
  	}
  	document.getElementById('verses').innerHTML = verses;
  }
  <!-- Verses check END -->

  <!-- Add sequence START -->
  function addSequence(event) {
      var targ = event.target || event.srcElement;
      document.getElementById('sequence').innerHTML += '<li>' + targ.innerHTML + '<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  }
  <!-- Add sequence END -->

  <!-- Smart add START -->
  function smartAdd() {
  	var lyrics = document.getElementById('songlyrics').value;
  	var sequence = "";
  	for (var i = 1; i <= 10; i++) {
  		if (lyrics.includes('[' + i + ']')) {
  			sequence += '<li>' + i + '<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  		}
  		if (lyrics.includes('[prechorus]') && lyrics.includes('[' + i + ']')) {
  			sequence += '<li>prechorus<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  		}
  		if (lyrics.includes('[chorus]') && lyrics.includes('[' + i + ']')) {
  			sequence += '<li>chorus<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  		}
  		if (lyrics.includes('[bridge]') && lyrics.includes('[' + i + ']') && i == 1) {
  			sequence += '<li>bridge<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  		}
  	}
  	if (lyrics.includes('[prechorus 2]') && lyrics.includes('[' + i + ']')) {
  		sequence += '<li>prechorus 2<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  	}
  	if (lyrics.includes('[chorus 2]') && lyrics.includes('[' + i + ']')) {
  		sequence += '<li>chorus 2<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  	}
  	if (lyrics.includes('[ending]')) {
  		sequence += '<li>ending<span class="w3-right"><button type="button" class="remove-item w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-times-circle"></i></button><button type="button" class="reorder-down  w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-down"></i></button><button type="button" class="reorder-up w3-margin-left w3-white" style="cursor:pointer;"><i class="fas fa-arrow-up"></i></button></span></li>';
  	}
  	document.getElementById('sequence').innerHTML += sequence;
  }
  <!-- Smart add END -->

  <!-- Clear sequence START -->
  function clearSequence() {
  	if (confirm("Are you sure you want to CLEAR the sequence?")) {
          document.getElementById('sequence').innerHTML = '';
      }
  }
  <!-- Clear sequence END -->

  <!-- Open tab START -->
  function openTab(tabName, elmnt) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
  		if (tablinks[i].classList.contains("w3-white")) {
  			tablinks[i].classList.remove("w3-white");
  			tablinks[i].classList.add("w3-blue");
  		}
    }
    document.getElementById(tabName).style.display = "initial";
HEREDOC;
	if(array_intersect($RL_SONGSFULL,$validRoles)) {
		echo <<<HEREDOC
		if (tabName == "playlisttab") {
			document.getElementById("musicsheettab").style.display = "initial";
			resizePlaylist();
		}
HEREDOC;
	} else {
		echo <<<HEREDOC
		if (tabName == "musiceditortab") {
			$('#songlistMember').show();
			$('#songlist').hide();
		} else {
			$('#songlistMember').hide();
			$('#songlist').show();
		}
HEREDOC;
	}
	echo <<<HEREDOC
	elmnt.classList.remove("w3-blue");
	elmnt.classList.add("w3-white");
	resizeSongList();
}
  <!-- Open tab END -->

  <!-- Reset form START -->
  function resetform() {
  	if (confirm("Are you sure you want to RESET the form?")) {
          songClear();
  		document.getElementById("musiceditorform").reset();
  		document.getElementById('verses').innerHTML = '';
  		document.getElementById('sequence').innerHTML = '';
      }
  }
  <!-- Reset form END -->

  <!-- Playlist add START -->
  function playlistAdd() {
  	if ({$songID} == -1) {
  		alert('Select a song before adding to playlist');
  	} else {
  		document.getElementById('playliststatus').innerHTML = '<b>Playlist Status: </b><b class="w3-text-black"><i class="fa fa-spinner fa-spin"></i></b>';
  		var xmlhttp = new XMLHttpRequest();
  		xmlhttp.onreadystatechange = function() {
  			if (this.readyState == 4 && this.status == 200) {
          document.getElementById('playliststatus').innerHTML = this.responseText;
  			}
  		};
HEREDOC;
echo <<<HEREDOC
  		xmlhttp.open("POST", "{$const['LOCATION']}include/worship_playlist_handler.php", true);
  		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  		xmlhttp.send("id=" + song.id + "&input=add");
  	}
  }
  <!-- Playlist add END -->

  <!-- Playlist empty START -->
  function playlistEmpty() {
  	if (confirm("Are you sure you want to EMPTY the playlist?")) {
          document.getElementById('playliststatus').innerHTML = '<b>Playlist Status: </b><b class="w3-text-black"><i class="fa fa-spinner fa-spin"></i></b>';
  		var xmlhttp = new XMLHttpRequest();
  		xmlhttp.onreadystatechange = function() {
  			if (this.readyState == 4 && this.status == 200) {
  				document.getElementById('playliststatus').innerHTML = this.responseText;
  			}
  		};
  		xmlhttp.open("POST", "{$const['LOCATION']}include/worship_playlist_handler.php", true);
  		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  		xmlhttp.send("input=empty");
      }
  }
  <!-- Playlist empty END -->

  function decodehsc(text) {
    var map = {
          '&amp;': '&',
          '&#038;': "&",
          '&lt;': '<',
          '&gt;': '>',
          '&quot;': '"',
          '&#039;': "'",
          '&#8217;': "’",
          '&#8216;': "‘",
          '&#8211;': "–",
          '&#8212;': "—",
          '&#8230;': "…",
          '&#8221;': '”'
    };
    return text.replace(/\&[\w\d\#]{2,5}\;/g, function(m) {
      return map[m];
    });
  }

  function resizePlaylist() {
    if($( window ).width() >= 584) {
      $('#plCurrentSong').css('height', $('#plWorshipPlaylist').css('height'));
    } else {
      $('#plCurrentSong').css('height', 'auto');
    }
  }

  <!-- When document loads finish -->
  $(document).ready(function(){

		var uploadModal = document.getElementById('songUpload');

 	 window.onclick = function(event) {
 	 	if (event.target == uploadModal) {
 			document.body.style.overflow = "auto";
 	 		uploadModal.style.display = "none";
 	 	}
 	 }

    $( window ).resize(function() {
      resizePlaylist();
    });

  	// On form submit
    $('#musiceditorform').submit(function(event){
	    // Cancel submit event
      event.preventDefault();
  		// Set variable "str" to input title
  		var str = $("input[name=title]").val();
  		// Display loading symbol
  		document.getElementById('savesubmitstatus').innerHTML = '<b><i class="fa fa-spinner fa-spin"></i></b>';
  		// If input "title" is empty
  		if (str.length == 0) {
  			// Call songClear function
  			songClear();
  			// Replace loading symbol with Error
  			document.getElementById('savesubmitstatus').innerHTML = '<b class="w3-text-red">Failed to submit, song title cannot be empty.</b>';
  			// Exit script
  			return;
  		// If input "title" is not empty
  		} else {
        document.getElementById('savesubmit').disabled = 'true';
				// Set variable "ul" to element whose id is "sequence"
				var ul = document.getElementById("sequence");
  			// Set variable "li" to elements in variable ul
  			var li = ul.getElementsByTagName("li");
  			// Set variable "lyrics" to input lyrics
  			var lyrics = document.getElementById('songlyrics').value;
  			// Set variable "seqdontexist" to false
  			var seqdontexist = false;
  			// Iterate times based on length of the variable "li"
  			for (var i = 0; i < li.length; i++) {
  				// Set variable "split" to string of the current "li" splitted at "<span"
  				var split = li[i].innerHTML.split("<span");
  				// If sequence contains a verse that doesn't exist in the input "lyrics"
  				if (lyrics.includes('[' + split[0] + ']') == false) {
  					// Set variable "seqdontexist" to true
  					seqdontexist = true;
            break;
  				}
  			}
HEREDOC;
echo <<<'HEREDOC'
  			if (seqdontexist == false) {
  				var datastring = $("#musiceditorform").serialize();
  				datastring = datastring.replace (/%0D%0A/g, "%0A");
  				// Query initialize
  				var dataList = "&sequence=";
  				// Find all li elements within the element with the id "sequence"
  				var $entries = $("#sequence").find('li');
  				// Declare variable "item" as String
  				var item = "";
  				<!-- Set variable "id" to song.id -->

HEREDOC;
echo <<<HEREDOC
  				var id = "id=" + {$songID} + "&";

HEREDOC;
echo <<<'HEREDOC'
  				$entries.each(function(i, items) {
  					// append the texts of the list items
  					for (var i = 1; i <= 10; i++) {
  						if ($(items).text() == i) {
  							item = i;
  						}
  					}
  					if ($(items).text() == "prechorus") {
  						item = "p";
  					} else if ($(items).text() == "chorus") {
  						item = "c";
  					} else if ($(items).text() == "bridge") {
  						item = "b";
  					} else if ($(items).text() == "prechorus 2") {
  						item = "q";
  					} else if ($(items).text() == "chorus 2") {
  						item = "t";
  					} else if ($(items).text() == "bridge 2") {
  						item = "w";
  					} else if ($(items).text() == "ending") {
  						item = "e";
  					}
  					dataList += item + ",";
  				});
HEREDOC;
echo <<<HEREDOC
  				dataList = dataList.substring(0, dataList.length - 1);
  				if ({$songID} == -1) {
  					var xmlhttp = new XMLHttpRequest();
  					xmlhttp.onreadystatechange = function() {
  						if (this.readyState == 4 && this.status == 200) {
  							songClear();
  							if (this.responseText.includes('successfully')) {
  								retrieveSongList();

HEREDOC;
								if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
									echo "memberSongList();";
								}
echo <<<HEREDOC
  								document.getElementById("deletesong").style.display = "initial";
  								document.getElementById("savesubmit").innerHTML = "<i class='fas fa-save'></i> Save";
                } else {
									document.getElementById("savesubmit").innerHTML = "<i class='fas fa-file-export'></i> Submit";
								}
                document.getElementById('savesubmitstatus').innerHTML = this.responseText;
								$('#savesubmit').removeAttr("disabled");
  						}
  					};
HEREDOC;
echo <<<HEREDOC
  					xmlhttp.open("POST", "{$const['LOCATION']}include/song_handler.php", true);
  					xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  					xmlhttp.send("input=submit&" + datastring + dataList);
  				} else {
  					var savehttp = new XMLHttpRequest();
  					savehttp.onreadystatechange = function() {
  						if (this.readyState == 4 && this.status == 200) {
  							songClear();
                document.getElementById('savesubmitstatus').innerHTML = this.responseText;
  							if (this.responseText.includes('success')) {
  								retrieveSongList();
HEREDOC;
								if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
									echo "memberSongList();";
								}
echo <<<HEREDOC
  								slideControl(0);
                  document.getElementById("currentsong").innerHTML = document.getElementById("musiceditorform").elements.namedItem("title").value;
                  document.getElementById('savesubmit').innerHTML = "<i class='fas fa-save'></i> Save";
                  document.getElementById('savesubmit').disabled = '';
  							}
  						}
  					};
  					savehttp.open("POST", "{$const['LOCATION']}include/song_handler.php", true);
  					savehttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  					savehttp.send("input=save" + "&" + id + datastring + dataList);
  				}
  			} else {
  				document.getElementById('savesubmitstatus').innerHTML = '<b class="w3-text-red">Error: A sequence does not exist in the lyrics.</b>';
          document.getElementById('savesubmit').disabled = '';
        }
  		}
    });
HEREDOC;
echo <<<'HEREDOC'
  	$("#sequence").on("click", ".reorder-up", function(){
  		var $current = $(this).closest('li');
  		var $previous = $current.prev('li');
  		if($previous.length !== 0){
  			$current.insertBefore($previous);
  		}
  	});

  	$("#sequence").on("click",".reorder-down", function(){
  		var $current = $(this).closest('li');
  		var $next = $current.next('li');
  		if($next.length !== 0){
  			$current.insertAfter($next);
  		}
  	});

  	$("#sequence").on("click",".remove-item", function(){
  		var $current = $(this).closest('li');
  		$current.remove();
  	});
HEREDOC;
if(array_intersect($RL_SONGSFULL,$validRoles)) {
	echo <<<'HEREDOC'
  	$("#worshipplaylist").on("click", ".playlist-move-up", function(){
  		document.getElementById('playliststatus').innerHTML = '<b>Playlist Status: </b><b class="w3-text-black"><i class="fa fa-spinner fa-spin"></i></b>';
  		var $current = $(this).closest('li');
  		var $val = $current.attr('name');
  		var xmlhttp = new XMLHttpRequest();
  		xmlhttp.onreadystatechange = function() {
  			if (this.readyState == 4 && this.status == 200) {
  				document.getElementById('playliststatus').innerHTML = this.responseText;
  			}
  		};
HEREDOC;
echo <<<HEREDOC
  		xmlhttp.open("POST", "{$const['LOCATION']}include/worship_playlist_handler.php", true);
  		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
HEREDOC;
echo <<<'HEREDOC'
  		xmlhttp.send("id=" + $val + "&input=up");
  	});

  	$("#worshipplaylist").on("click",".playlist-move-down", function(){
  		document.getElementById('playliststatus').innerHTML = '<b>Playlist Status: </b><b class="w3-text-black"><i class="fa fa-spinner fa-spin"></i></b>';
  		var $current = $(this).closest('li');
  		var $val = $current.attr('name');
  		var xmlhttp = new XMLHttpRequest();
  		xmlhttp.onreadystatechange = function() {
  			if (this.readyState == 4 && this.status == 200) {
  				document.getElementById('playliststatus').innerHTML = this.responseText;
  			}
  		};
HEREDOC;
echo <<<HEREDOC
  		xmlhttp.open("POST", "{$const['LOCATION']}include/worship_playlist_handler.php", true);
  		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
HEREDOC;
echo <<<'HEREDOC'
  		xmlhttp.send("id=" + $val + "&input=down");
  	});

  	$("#worshipplaylist").on("click",".playlist-delete", function(){
  		document.getElementById('playliststatus').innerHTML = '<b>Playlist Status: </b><b class="w3-text-black"><i class="fa fa-spinner fa-spin"></i></b>';
  		var $current = $(this).closest('li');
  		var $val = $current.attr('name');
  		var xmlhttp = new XMLHttpRequest();
  		xmlhttp.onreadystatechange = function() {
  			if (this.readyState == 4 && this.status == 200) {
  				document.getElementById('playliststatus').innerHTML = this.responseText;
  			}
  		};
HEREDOC;
echo <<<HEREDOC
  		xmlhttp.open("POST", "{$const['LOCATION']}include/worship_playlist_handler.php", true);
  		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
HEREDOC;
echo <<<'HEREDOC'
  		xmlhttp.send("id=" + $val + "&input=delete");
  	});
HEREDOC;
}
echo <<<HEREDOC
  });
HEREDOC;
	if(array_intersect($RL_SONGSFULL,$validRoles)) {
	echo <<<HEREDOC
  if (!!window.EventSource) {
      var source = new EventSource("{$const['LOCATION']}include/worship_sse.php");
      source.addEventListener('playlistControl', function(e) {
        if (document.getElementById("worshipplaylist").innerHTML != e.data) {
          document.getElementById("worshipplaylist").innerHTML = e.data;
        }
      }, false);
  } else {
      document.getElementById("worshipplaylist").innerHTML = "Sorry, your browser does not support server-sent events...";
  }
HEREDOC;
	}
}

?>
</script>
<!-- Scripts END -->

</head>
<body onload="retrieveSongList();<?php if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {echo "memberSongList();";} ?>" class="w3-animate-opacity">
<?php require_once 'include/header.php'; ?>
<div id="songUpload" class="w3-modal" style="z-index:4">
  <div class="w3-modal-content w3-animate-opacity w3-display-middle" style="margin-top:48px;margin-left:0;max-width:90vw;">
    <div class="w3-bar w3-black">
      <span onclick="document.getElementById('songUpload').style.display='none';$('html').css('overflow','auto');retrieveSongList();" class="w3-button w3-display-topright w3-black">&times;</span>
      <button class="w3-bar-item w3-button">EASISLIDES</button>
    </div>
    <div id="easislides" class="w3-panel">
      <iframe name="suFrame" class="w3-bar"></iframe>
      <p>Currently only supports Easislides exported XML.</p>
      <form enctype="multipart/form-data" action="<?php echo $const['LOCATION']; ?>include/song_upload.php" target="suFrame" method="post" class="w3-section">
        <input type="file" name="fileToUpload" id="fileToUpload" accept=".xml"></input>
        <br><br>
        <input type="submit" value="Upload"></input>
      </form>
    </div>
  </div>
</div>
<div class="w3-cell-row">
  <!-- Song List -->
	<div class="w3-sidebar w3-collapse w3-animate-left w3-col l4 m6" style="z-index:3;position:absolute;overflow-y:hidden;" id="slSidebar">
    <div class="w3-container w3-blue w3-card">
      <h2 class="w3-wide w3-center"><label>Song List</label></h2>
    </div>
		<a class="w3-button w3-block w3-card w3-hide-large w3-large w3-red" href="javascript:void(0)" onclick="close_sl()"><i class="fas fa-times"> Close</i></a>
    <div class="w3-container w3-section">
      <input class="w3-input w3-border w3-padding" type="text" placeholder="Search for songs by names.." id="songinput" onkeyup="songSearch()">
        <ul class="w3-ul w3-hoverable w3-container" id="songlist" onclick="selectSong(event)" style="cursor:pointer;overflow-y:auto;width:100%;">
        </ul>
				<?php
				if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
					echo <<<HEREDOC
					<ul class="w3-ul w3-hoverable w3-container" id="songlistMember" onclick="memberSelectSong(event)" style="cursor:pointer;overflow-y:auto;width:100%;display:none;">
	        </ul>
HEREDOC;
				}
				?>
    </div>
  </div>
	<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="close_sl()" style="cursor:pointer" id="slOverlay"></div>
  <div class="w3-main w3-animate-left" style="margin-left:33%;" id="content">
    <div class="w3-container w3-blue w3-card">
      <h2 class="w3-wide w3-center">
<?php
if(array_intersect($RL_SONGSFULL,$validRoles)) {
echo <<<'HEREDOC'
        <!-- Tab List -->
				<button type="button" class="w3-button w3-hide-large w3-col m3 w3-blue-gray w3-card" style="padding:0px; margin-bottom:10px;" onclick="open_sl();"><i class="fas fa-clipboard-list"></i> Songs</button>
				<button type="button" class="tablink w3-button w3-col m3 l4 w3-white w3-card" style="padding:0px; margin-bottom:10px;" onclick="openTab('musicsheettab', this)"><i class="fas fa-th-list"></i> Sheet</button>
				<button type="button" class="tablink w3-button w3-col m3 l4 w3-blue w3-card" style="padding:0px; margin-bottom:10px;" onclick="openTab('musiceditortab', this)"><i class="fas fa-th-list"></i> Editor</button>
				<button type="button" class="tablink w3-button w3-col m3 l4 w3-blue w3-card" style="padding:0px; margin-bottom:10px;" onclick="openTab('playlisttab', this)"><i class="fas fa-th-list"></i> Playlist</button>
HEREDOC;
} else if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
	echo <<<'HEREDOC'
	        <!-- Tab List -->
					<button type="button" class="w3-button w3-hide-large w3-col m4 w3-blue-gray w3-card" style="padding:0px; margin-bottom:10px;" onclick="open_sl();"><i class="fas fa-clipboard-list"></i> Songs</button>
					<button type="button" class="tablink w3-button w3-col m4 l6 w3-white w3-card" style="padding:0px; margin-bottom:10px;" onclick="openTab('musicsheettab', this)"><i class="fas fa-th-list"></i> Sheet</button>
					<button type="button" class="tablink w3-button w3-col m4 l6 w3-blue w3-card" style="padding:0px; margin-bottom:10px;" onclick="openTab('musiceditortab', this)"><i class="fas fa-th-list"></i> Editor</button>
HEREDOC;
} else {
echo <<<'HEREDOC'
        <button type="button" class="w3-button w3-hide-large w3-col m6 w3-blue-gray w3-card" style="padding:0px; margin-bottom:10px;" onclick="open_sl();"><i class="fas fa-clipboard-list"></i> Songs</button>
			  <label class="w3-col m6 l12" style="padding:0px; margin-bottom:10px;">Music Sheet</label>
HEREDOC;
}
echo <<<HEREDOC
      </h2>
    </div>
HEREDOC;
if(array_intersect($RL_SONGSFULL,$validRoles)) {
echo <<<'HEREDOC'
    <!-- Playlist -->
    <div id="playlisttab" class="tabcontent w3-animate-opacity" style="display:none;">
      <div class="w3-row-padding">
				<div class="w3-container w3-col m4 l4">
					<div class="w3-panel w3-border w3-border-blue w3-center" id="plCurrentSong">
						<p>
							<label style="font-size:16px;" class="w3-text-blue"><b>Current song: </b><b class="w3-text-green" id="sm_selectedsong">None</b></label>
						</p>
						<table class="w3-center w3-panel" style="width:100%;">
							<tr>
								<td>
									<label class="w3-text-teal"><b>Add to<br>playlist</b></label>
									<p>
										<button class="w3-btn w3-teal w3-round-small w3-card" type="button" onclick="playlistAdd();"><i class="fas fa-plus"></i></button>
									</p>
								</td>
								<td>
									<label class="w3-text-red"><b>Empty<br>playlist</b></label>
									<p>
										<button class="w3-btn w3-red w3-round-small w3-card" type="button" onclick="playlistEmpty();"><i class="fas fa-trash-alt"></i></button>
									</p>
								</td>
							</tr>
						</table>
						<p>
							<label class="w3-text-blue" id="playliststatus"></label>
						</p>
					</div>
				</div>
        <div class="w3-container w3-col m8 l8">
          <div class="w3-panel w3-border w3-border-blue" id="plWorshipPlaylist">
            <p class="w3-center">
            <label class="w3-text-blue" style="font-size:16px;"><b>Worship playlist</b></label>
            </p>
            <div class="w3-border w3-panel" style="overflow-y:auto; height:16em;">
              <ul class="w3-ul w3-hoverable w3-margin" id="worshipplaylist" onclick="selectSong(event)" style="cursor:pointer;">
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
HEREDOC;
}
?>
  	<!-- Music Sheet -->
		<div id="musicsheettab" class="tabcontent w3-animate-opacity">
      <div class="w3-container">
        <?php require_once 'include/song_sheet.php'; ?>
      </div>
  	</div>
    <?php
if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_SONGSLIMIT,$validRoles)) {
echo <<<'HEREDOC'
    <!-- Music Editor -->
    <div id="musiceditortab" class="tabcontent w3-animate-opacity" style="display:none;">
      <form class="w3-container" id="musiceditorform">
        <div class="w3-container">
          <p>
            <label style="font-size:32px;" class="w3-text-blue"><b>Current song: </b><b class="w3-text-green" id="currentsong">Creating a new song</b></label>
          </p>
          <p>
            <label class="w3-text-blue"><b>Song Title</b></label>
            <input class="w3-input w3-border" name="title" type="text" placeholder="Enter song title here..." onkeyup="songCheck(this.value)">
          </p>
          <p>
            <label id="songstatus"></label>
          </p>
          <p>
            <label class="w3-text-blue"><b>Writer</b></label>
            <input class="w3-input w3-border" name="writer" type="text" placeholder="Enter writer name here...">
          </p>
        </div>
        <div style="margin-left:12px; margin-right:12px;">
					<table style="width:100%;">
						<tr style="text-align:center;">
							<td style="width:33.33333%"><label class="w3-text-blue"><b>Song No.</b></label></td>
							<td style="width:33.33333%"><label class="w3-text-blue"><b>Key</b></label></td>
							<td style="width:33.33333%"><label class="w3-text-blue"><b>Transpose Key</b></label></td>
						</tr>
						<tr>
							<td><input class="w3-input w3-border" name="songno" type="number" value="0" placeholder="Enter song no. here..."></td>
						  <td><select class="w3-select w3-border" name="songkey">
	              <option value="" hidden>Select a key</option>
	              <option value="A">A</option>
	              <option value="B">B</option>
	              <option value="C">C</option>
	              <option value="D">D</option>
	              <option value="E">E</option>
	              <option value="F">F</option>
	              <option value="G">G</option>
	              <option value="Am">Am</option>
	              <option value="Bm">Bm</option>
	              <option value="Cm">Cm</option>
	              <option value="Dm">Dm</option>
	              <option value="Em">Em</option>
	              <option value="Fm">Fm</option>
	              <option value="Gm">Gm</option>
	              <option value="Ab">Ab</option>
	              <option value="Bb">Bb</option>
	              <option value="Cb">Cb</option>
	              <option value="Db">Db</option>
	              <option value="Eb">Eb</option>
	              <option value="F#">F#</option>
	              <option value="Bbm">Bbm</option>
	              <option value="C#m">C#m</option>
	              <option value="D#m">D#m</option>
	              <option value="E#m">E#m</option>
	              <option value="F#m">F#m</option>
	              <option value="G#m">G#m</option>
	            </select></td>
							<td>
							<select class="w3-select w3-border" name="transpose">
								<option value="">None</option>
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<option value="D">D</option>
								<option value="E">E</option>
								<option value="F">F</option>
								<option value="G">G</option>
								<option value="Am">Am</option>
								<option value="Bm">Bm</option>
								<option value="Cm">Cm</option>
								<option value="Dm">Dm</option>
								<option value="Em">Em</option>
								<option value="Fm">Fm</option>
								<option value="Gm">Gm</option>
								<option value="Ab">Ab</option>
								<option value="Bb">Bb</option>
								<option value="Cb">Cb</option>
								<option value="Db">Db</option>
								<option value="Eb">Eb</option>
								<option value="F#">F#</option>
								<option value="Bbm">Bbm</option>
								<option value="C#m">C#m</option>
								<option value="D#m">D#m</option>
								<option value="E#m">E#m</option>
								<option value="F#m">F#m</option>
								<option value="G#m">G#m</option>
							</select>
							</td>
						</tr>
					</table>
        </div>
        <div class="w3-container">
          <p>
            <label class="w3-text-blue"><b>Song Lyrics</b></label>
            <textarea class="w3-input w3-border" style="resize:none;" name="lyrics" id="songlyrics" rows="6" placeholder="Enter Song lyrics here..." onkeyup="versesCheck()"></textarea>
          </p>
          <br>
          <p>
            <label class="w3-text-blue"><b>Song Lyric Buttons</b></label><br/>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="1">1</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="2">2</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="3">3</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="4">4</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="5">5</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="6">6</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="7">7</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="8">8</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="9">9</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Verse Indicator" type="button" onclick="textAdd(value)" value="10">10</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Prechorus Indicator" type="button" onclick="textAdd(value)" value="prechorus">Pr</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Prechorus 2 Indicator" type="button" onclick="textAdd(value)" value="prechorus 2">Pr<sub>2</sub></button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Chorus Indicator" type="button" onclick="textAdd(value)" value="chorus">Ch</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Chorus 2 Indicator" type="button" onclick="textAdd(value)" value="chorus 2">Ch<sub>2</sub></button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Bridge Indicator" type="button" onclick="textAdd(value)" value="bridge">Br</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Bridge 2 Indicator" type="button" onclick="textAdd(value)" value="bridge 2">Br<sub>2</sub></button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Ending Indicator" type="button" onclick="textAdd(value)" value="ending">...</button>
            <button class="w3-button w3-teal w3-margin-bottom w3-round-small w3-card" title="Chords Indicator" type="button" onclick="textAdd(value)" value="♮">♮</button>
						<button class="w3-button w3-blue w3-margin-bottom w3-round-small w3-card" type="button" title="Smart Add" onclick="smartAdd()"><i class="fas fa-plus"></i> Smart Add</button>
						<button class="w3-button w3-red w3-margin-bottom w3-round-small w3-card" type="button" title="Clear" onclick="clearSequence()"><i class="fas fa-trash-alt"></i> Clear</button>
					</p>
        </div>
        <div class="w3-row-padding w3-container">
          <div class="w3-half w3-center w3-blue w3-border">
            <label><b>Verses</b></label>
            <div class="w3-white" style="height:16em; overflow-y:auto;">
              <ul id="verses" class="w3-ul w3-hoverable w3-border" onclick="addSequence(event)" style="cursor:pointer;">
              </ul>
            </div>
            <br>
          </div>
          <div class="w3-half w3-center w3-blue w3-border">
            <label><b>Sequence</b></label>
            <div class="w3-white" style="height:16em; overflow-y:auto;">
              <ul id="sequence" class="w3-ul w3-hoverable w3-border">
              </ul>
            </div>
            <br>
          </div>
        </div>
        <div class="w3-container">
					<span class="w3-text-blue w3-col" id="savesubmitstatus"></span>
          <div>
            <button class="w3-btn w3-blue w3-right w3-section w3-margin-left w3-round-small w3-card" type="submit" id="savesubmit"><i class="fas fa-file-export"></i> Submit</button>
            <button class="w3-btn w3-red w3-right w3-section w3-margin-left w3-round-small w3-card" style="display:none;" type="button" onclick="songDelete()" id="deletesong"><i class="fas fa-trash-alt"></i> Delete</button>
            <button class="w3-btn w3-pink w3-right w3-section w3-margin-left w3-round-small w3-card" type="button" onclick="resetform()"><i class="fas fa-eraser"></i> Reset</button>
						<button class="w3-btn w3-green w3-right w3-section w3-margin-left w3-round-small w3-card" type="button" onclick="document.getElementById('songUpload').style.display='block';$('html').css('overflow','hidden');"><i class="fas fa-file-upload"></i> Upload</button>
					</div>
        </div>
      </form>
    </div>
HEREDOC;
}
?>
		</div>
	</div>
</div>
<?php require_once 'include/footer.php'; ?>
<?php
if($_SERVER['REQUEST_METHOD'] == "GET") {
  if(isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])) {
    echo <<<HEREDOC
    <script type="text/javascript">
HEREDOC;
		if(!array_intersect($RL_SONGSFULL,$validRoles) && array_intersect($RL_SONGSLIMIT,$validRoles)) {
			echo 'var getReq = true;';
		}
		echo <<<HEREDOC
    retrieveSong({$_GET['id']});
    </script>
HEREDOC;
  }
}
?>
</body>
</html>
