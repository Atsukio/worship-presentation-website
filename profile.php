<?php
require_once 'include/head.php';

$ppNName = $ppUser = $ppEmail = $ppRoles = '';
if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET['user']) && !empty($_GET['user'])) {
  $sql = "SELECT nickname, username, email, avatar, roles, description, joined_date
  FROM " . TB_USERS . "
  WHERE username = ?";
	$ps = $mysqli->prepare($sql);
  $ps->bind_param("s", $_GET['user']);
  if(!$ps->execute()) {
    die("ERROR: Problem occured while finding user");
  }
  $row = $ps->get_result()->fetch_assoc();
  if(!empty($row['username'])) {
    $ppNName = htmlspecialchars($row['nickname']);
    $ppUser = htmlspecialchars($row['username']);
    $ppRoles = htmlspecialchars($row['roles']);
    if(empty($row['avatar'])) {
      $ppAvatar = $const['D_AVATAR'] . "?" . time();
      $ppAvatarAlt = "blank_avatar";
    } else {
      $ppAvatar = htmlspecialchars($const['AVATARS'] . $row['avatar'] . "?" . time());
      $ppAvatarAlt = htmlspecialchars($row['avatar']);
    }
    $ppDesc = htmlspecialchars($row['description']);
    $ppJDate = $row['joined_date'];
    echo <<<HEREDOC
    </head>
    <body class="w3-animate-opacity">
HEREDOC;
      require_once 'include/header.php';
      echo <<<HEREDOC
      <h2 class="w3-topbar w3-bottombar w3-wide w3-blue w3-border-pale-blue w3-center w3-container"
      style="margin:0;"><b>{$ppUser}'s Profile Page</b></h2>
      <div class="w3-container w3-section" style="min-height:70vh;">
        <div class="w3-col l3 m4">
          <div class="w3-display-container w3-col">
            <p class="w3-col m1 s4"></p>
            <img class="w3-image w3-container w3-padding-16 w3-border w3-round w3-col m10 s4" src="{$ppAvatar}" alt="{$ppAvatarAlt}" />
          </div>
          <div class="w3-padding-large w3-center">
            <span class="w3-text-teal"><b>Username:</b></span><span class="w3-text-blue"><b> {$ppUser}</b></span><br />
            <span class="w3-text-teal"><b>Member since:</b></span><span class="w3-text-blue"><b> {$ppJDate}</b></span><br />
HEREDOC;
          require_once 'include/roles.php';
echo<<<HEREDOC
          </div>
        </div>
        <div class="w3-col l9 m8">
          <h2 class="w3-text-teal" for="ppNName">Nickname:</h2>
          <h4 class="w3-text-black">{$ppNName}</h4><br />
          <h2 class="w3-text-teal" for="ppDescription">Description:</h2>
          <h4 class="w3-text-black">{$ppDesc}</h4>
        </div>
      </div>
HEREDOC;
      require_once 'include/footer.php';
      echo <<<HEREDOC
    </body>
HEREDOC;
  } else {
    echo <<<HEREDOC
    </head>
    <body class="w3-animate-opacity">
HEREDOC;
    require_once 'include/header.php';
    echo <<<HEREDOC
      <div class="w3-padding-large" style="min-height:80vh">
        <h1 class="w3-text-red w3-center w3-padding-large">ERROR 404: No such user exists</h1>
        <h3 class="w3-center w3-padding-large">If you believe this is an error, contact the website administrator</h3>
      </div>
HEREDOC;
    require_once 'include/footer.php';
    echo <<<HEREDOC
    </body>
HEREDOC;
  }
  die();
} else if($validToken) {
  $sql = "SELECT nickname, username, email, roles, avatar, description, joined_date
  FROM " . TB_USERS . "
  WHERE token = ?";
	$ps = $mysqli->prepare($sql);
  $ps->bind_param("s", $_COOKIE[CK_TOKEN]);
  if(!$ps->execute()) {
    die("ERROR: Problem occured while finding user");
  }
  $row = $ps->get_result()->fetch_assoc();
  $ppNName = htmlspecialchars($row['nickname']);
  $ppUser = htmlspecialchars($row['username']);
  $ppRoles = htmlspecialchars($row['roles']);
  if(empty($row['avatar'])) {
    $ppAvatar = $const['D_AVATAR'];
    $ppAvatarAlt = "blank_avatar";
  } else {
    $ppAvatar = htmlspecialchars($const['AVATARS'] . $row['avatar'] . "?" . time());
    $ppAvatarAlt = htmlspecialchars($row['avatar']);
  }
  $ppDesc = htmlspecialchars($row['description']);
  $ppJDate = $row['joined_date'];
} else {
echo <<<HEREDOC
</head>
<body class="w3-animate-opacity">
HEREDOC;
require_once 'include/header.php';
echo <<<HEREDOC
  <div class="w3-padding-large" style="min-height:80vh">
    <h1 class="w3-text-red w3-center w3-padding-large">No permission to view this page</h1>
    <h3 class="w3-center w3-padding-large">You must be logged in to view this page.</h3>
  </div>
HEREDOC;
require_once 'include/footer.php';
echo <<<HEREDOC
</body>
HEREDOC;
die();
}
?>
<script>
function saveProfile() {
  var datastring = $('#ppForm').serialize();
  datastring = datastring.replace (/%0D%0A/g, "%0A");
  xmlhttp = new XMLHttpRequest;
  xmlhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      document.getElementById('ppSaveStatus').innerHTML = this.responseText;
      if(this.responseText.includes("w3-text-green")) {
        document.getElementById('ppCPass').value = '';
        document.getElementById('ppNPass').value = '';
      }
    }
  };
  xmlhttp.open("POST", "<?php echo $const['LOCATION']; ?>include/profile_handler.php", true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send(datastring);
}

$(document).ready(function (e) {

  var uploadModal = document.getElementById('uploadModal');

  window.onclick = function(event) {
   if (event.target == uploadModal) {
     document.body.style.overflow = "auto";
     uploadModal.style.display = "none";
   }
  }

 $("#avatarForm").on('submit',(function(e) {
  e.preventDefault();
  $.ajax({
         url: "<?php echo $const['LOCATION']; ?>include/profile_handler.php",
   type: "POST",
   data:  new FormData(this),
   contentType: false,
         cache: false,
   processData:false,
   beforeSend : function() {
    //$("#preview").fadeOut();
    $("#avatarUploadError").fadeOut();
   },
   success: function(data) {
    if(data=='invalid') {
     // invalid file format.
     $("#avatarUploadError").html("Invalid File !").fadeIn();
   } else {
     // view uploaded file.
     d = new Date();
     $("#avatarImg").attr("src", "<?php echo $const['AVATARS']; ?>" + data+"?"+d.getTime()).fadeIn();
     $("#avatarImg").attr("alt", data).fadeIn();
     $("#userAvatar").attr("src", "<?php echo $const['AVATARS']; ?>" + data+"?"+d.getTime()).fadeIn();
     $("#userAvatar").attr("alt", data).fadeIn();
     $("#avatarForm")[0].reset();
     $("#uploadModal").css("display","none");
     $("body").css("overflow","auto");
    }
  },
  error: function(e) {
    $("#avatarUploadError").html(e).fadeIn();
  }
  });
 }));
});
</script>
</head>
<body class="w3-animate-opacity">
  <?php require_once 'include/header.php'; ?>
  <div id="uploadModal" class="w3-modal" style="position:fixed;">
    <div class="w3-modal-content w3-animate-opacity w3-card-4 w3-display-middle" style="margin-top:48px;margin-left:0;">
      <header class="w3-container w3-teal">
        <span onclick="document.getElementById('uploadModal').style.display='none';document.body.style.overflow='auto';"
        class="w3-button w3-display-topright">&times;</span>
        <h2>Change Avatar</h2>
      </header>
      <div class="w3-container" style="overflow:auto;max-width:90vw;max-height:25vh;">
        <p>
          <form id="avatarForm">
            <input type="file" name="avatar" accept=".png, .jpg, .jpeg" /><br />
            <p><button class="w3-button w3-green" type="submit" size="2000000">Upload</button></p>
            <div id="avatarUploadError"></div>
          </form>
        </p>
      </div>
      <footer class="w3-container w3-teal">
        <p>Choose a file to upload as your new avatar. <span class="w3-text-red"><b>(Max size: 2MB)</b></span></p>
      </footer>
    </div>
  </div>
  <h2 class="w3-topbar w3-bottombar w3-wide w3-blue w3-border-pale-blue w3-center w3-container"
  style="margin:0;"><b>Edit Profile Page</b></h2>
  <div class="w3-container w3-section" style="min-height:70vh;">
    <div class="w3-col l3 m4">
      <div class="w3-display-container w3-col">
        <p class="w3-col m1 s4"></p>
        <img class="w3-circle w3-image w3-container w3-padding-16 w3-border w3-round w3-col m10 s4" id="avatarImg" src="<?php echo $ppAvatar; ?>" alt="<?php echo $ppAvatar; ?>" />
        <button class="w3-btn w3-display-middle w3-white w3-card w3-display-hover"
        onclick="document.getElementById('uploadModal').style.display='block';document.body.style.overflow='hidden';">
          <i class="fas fa-images"></i> Change Avatar
        </button>
      </div>
      <div class="w3-padding-large w3-center">
        <span class="w3-text-teal"><b>Username:</b></span><span class="w3-text-blue"><b> <?php echo $ppUser; ?></b></span><br />
        <span class="w3-text-teal"><b>Member since:</b></span><span class="w3-text-blue"><b> <?php echo $ppJDate; ?></b></span><br />
        <?php require_once 'include/roles.php'; ?>
      </div>
    </div>
    <div class="w3-col l9 m8">
      <form id="ppForm" class="w3-panel">
        <label class="w3-text-teal" for="ppNName"><b>Nickname:</b></label>
        <input class="w3-input w3-border w3-hover-light-gray" id="ppNName" name="nName"
        type="text" value="<?php echo $ppNName; ?>" placeholder="Enter nickname here (optional)"/><br />
        <label class="w3-text-teal" for="ppDescription"><b>Description:</b></label>
        <textarea class="w3-input w3-border" id="ppDescription" name="description" placeholder="Profile description here..."><?php echo $ppDesc; ?></textarea><br />
        <label class="w3-text-teal" for="ppEmail"><b>Email address:</b></label>
        <input class="w3-input w3-border w3-hover-light-gray" id="ppEmail" name="email" type="email"
        value="<?php echo $ppEmail; ?>" placeholder="Enter email address here (optional)"/><br />
        <label class="w3-text-teal" for="ppCPass"><b>Current password:</b></label>
        <input class="w3-input w3-border w3-hover-light-gray" id="ppCPass" name="cPass"
        type="password" placeholder="Only enter current password when entering a new password"/><br />
        <label class="w3-text-teal" for="ppNPass"><b>New password:</b></label>
        <input class="w3-input w3-border w3-hover-light-gray" id="ppNPass" name="nPass"
        type="password" placeholder="Enter new password here (optional)"/><br />
        <label class="w3-left" id="ppSaveStatus"></label>
        <button class="w3-btn w3-right w3-green" type="button" onclick="saveProfile();">
          <i class="fas fa-save"></i> Save
        </button>
      </form>
    </div>
  </div>
  <?php require_once 'include/footer.php'; ?>
</body>
