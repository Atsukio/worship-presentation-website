<?php
require_once 'include/config.php';
if(!isset($_COOKIE[CK_LIVE_SONG])) {
	setcookie(CK_LIVE_SONG, '-1', time() + (86400), "/");
}
if(!isset($_COOKIE[CK_LIVE_SEQ])) {
	setcookie(CK_LIVE_SEQ, '-1', time() + (86400), "/");
}
require_once 'include/head.php';
?>
<?php require_once 'include/song_sheet_scripts.php'; ?>
<script>
var WorshipStatus;
var HostRefresh;
<?php require_once 'include/cookies_script.php'; ?>
<!-- Slide control START -->
function slideControl(num) {
	document.getElementById('sheetlyrics').innerHTML = '';
	document.getElementById('seqinfo').innerHTML = '';
	var item = "", tempLyrics = "", temp = "";
	if (sheet.sequence == '') {
		tempLyrics = sheet.lyrics;
  } else {
		if (parseInt(sheet.currentSequence + num) >= 0 && parseInt(sheet.currentSequence + num) < parseInt(sheet.sequence.length)) {
			if(!document.getElementById('followmode').checked || document.getElementById('worshipStatus').innerHTML.includes('<span class="w3-mobile">OFF-AIR</span>')) {
				sheet.currentSequence = parseInt(sheet.currentSequence + num);
			}
		}

<?php
if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_BROADCAST,$validRoles)) {
		echo <<<HEREDOC
		if(document.getElementById('worshipStatus').innerHTML.includes("<b><u>$validUser</u></b>") && !HostRefresh) {
			if(document.hasFocus()) {
				setCookie("{$const['CK_LIVE_SEQ']}", sheet.currentSequence, 1);
				var hosthttp = new XMLHttpRequest();
				// hosthttp.onreadystatechange = function() {
				//   if(this.readyState == 4 && this.status == 200) {
				//     alert(this.responseText);
				//   }
				// };
				hosthttp.open("POST", "include/broadcast_handler.php", true);
				hosthttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				hosthttp.send("input=sequence&seq=" + sheet.currentSequence);
			}
		}
HEREDOC;
}
?>

		document.getElementById("prevslidebtn").style.display = "none";
		document.getElementById("nextslidebtn").style.display = "none";

		if (!document.getElementById('scrollmode').checked) {
      tempLyrics = sheet.lyrics[sheet.currentSequence];
			if (sheet.currentSequence == 0) {
				document.getElementById("prevslidebtn").style.display = "none";
			} else {
				if(document.getElementById('followmode').checked &&
				document.getElementById('worshipStatus').innerHTML.includes('<span class="w3-mobile">ON-AIR</span>')) {
					document.getElementById("prevslidebtn").style.display = "none";
				} else {
					document.getElementById("prevslidebtn").style.display = "";
				}
			}
			if (sheet.currentSequence == sheet.sequence.length - 1 || Object.keys(sheet.currentSequence).length !== 0) {
        document.getElementById("nextslidebtn").style.display = "none";
			} else {
				if(document.getElementById('followmode').checked &&
				document.getElementById('worshipStatus').innerHTML.includes('<span class="w3-mobile">ON-AIR</span>')) {
					document.getElementById("nextslidebtn").style.display = "none";
				} else {
					document.getElementById("nextslidebtn").style.display = "";
				}
			}
			document.getElementById("seqinfo").innerHTML = '';
			for (var i = 0; i < sheet.sequence.length; i++) {
				if (sheet.currentSequence == i) {
					document.getElementById("seqinfo").innerHTML += "<h5 class='w3-badge w3-green' style='display:inline-block; font-size:4vmin;'>" + sheet.sequence[sheet.currentSequence] + "</h5>";
				} else {
					document.getElementById("seqinfo").innerHTML += " " + sheet.sequence[i];
				}
			}
		} else {
			tempLyrics = '';
			for (var i = 0; i < sheet.lyrics.length; i++) {
				tempLyrics += sheet.lyrics[i] + "<br><br>";
			}
			for (var i = 0; i < sheet.sequence.length; i++) {
					document.getElementById("seqinfo").innerHTML += " " + sheet.sequence[i];
			}
		}
	}

	if (document.getElementById("showchords").checked) {
			if (song.key != "") {
				if (song.transpose != "") {
					document.getElementById("songdesc").innerHTML = song.title +
					"<span class='w3-text-gray'> [KEY: " + song.transpose + "]</span>";
				} else {
					document.getElementById("songdesc").innerHTML = song.title +
					"<span class='w3-text-gray'> [KEY: " + song.key + "]</span>";
				}
				tempLyrics = tempLyrics.replace(/display:none;/g, "");
			}
	} else {
		document.getElementById("songdesc").innerHTML = song.title;
	}
	document.getElementById("sheetlyrics").innerHTML = tempLyrics;
}
<!-- Slide control END -->

<!-- Select song START -->
function selectSong(event) {
	var targ = event.target || event.srcElement;
	// Start of Music Sheet
	document.getElementById('sheetlyrics').innerHTML = '';
	document.getElementById('songdesc').innerHTML = '';
	document.getElementById('seqinfo').innerHTML = '';
	// End of Music Sheet
  retrieveSong(targ.value);
}
<!-- Select song END -->

<!-- Retrieve song START -->
function retrieveSong(id) {
<?php
if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_BROADCAST,$validRoles)) {
$const = get_defined_constants();
echo <<<HEREDOC
if(document.getElementById('worshipStatus').innerHTML.includes("Broadcasting By <b><u>$validUser</u></b>") && !HostRefresh) {
	setCookie("{$const['CK_LIVE_SONG']}", id, 1);
	var hosthttp = new XMLHttpRequest();
	// hosthttp.onreadystatechange = function() {
	//   if(this.readyState == 4 && this.status == 200) {
	//     alert(this.responseText);
	//   }
	// };
	hosthttp.open("POST", "include/broadcast_handler.php", true);
	hosthttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	hosthttp.send("input=song&id=" + id);
}
HEREDOC;
}
?>
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = this.responseText.split("~split~");
			// Start of Music Sheet
			sheet.currentSequence = 0;
			song.id = id;
			song.title = result[0];
			song.writer = result[1];
			song.no = result[2];
			song.key = result[3];
			song.transpose = result[4];
			song.lyrics = result[5];
      sheet.sequence = result[6].split(",");
			sheet.lyrics = song.lyrics.replace(/\n/g, "<br>");
			document.getElementById("songdesc").innerHTML = song.title;
			$("#writerinfo").html(song.writer);
      sheetLyrics();
      slideControl(0);
			// End of Music Sheet
		}
	};
	xmlhttp.open("POST", "include/song_handler.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("input=select&id=" + id);
}
<!-- Retrieve song END -->

<?php
if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_BROADCAST,$validRoles)) {
echo <<<HEREDOC
function broadcastControls(v) {
  if (v == 'start' || v == 'end' || v == 'override') {
    if(v == 'end' || v == 'override') {
      if(confirm("Are you sure? Press OK to confirm") != true) {
        return;
      }
    } else if(v == 'start') {
			if($( window ).width() > 600) {
	      window.open("/worship.php","","fullscreen=yes");
	    }
		}
    var xmlhttp = new XMLHttpRequest;
    xmlhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
        if(this.responseText != 'success') {
          alert(this.responseText);
        }
      }
    };
    xmlhttp.open("POST", "include/broadcast_handler.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("input=" + v + "&song=" + song.id + "&seq=" + sheet.currentSequence);
  } else {
    alert("Error: Something went wrong, please refresh the page.")
    return;
  }
}

$( document ).ready(function() {
	var checkCookie = function() {
		if(WorshipStatus === "ON-AIR") {
			if (getCookie("{$const['CK_LIVE_SONG']}") !== song.id && !document.hasFocus()) {
	        retrieveSong(getCookie("{$const['CK_LIVE_SONG']}"));
	    }
			if (getCookie("{$const['CK_LIVE_SEQ']}") != sheet.sequence && !document.hasFocus()) {
				sheet.currentSequence = parseInt(getCookie("{$const['CK_LIVE_SEQ']}"));
				slideControl(0);
	    }
		}
	};

	window.setInterval(checkCookie, 250);
});

HEREDOC;

}

?>

if (!!window.EventSource) {
    var source = new EventSource("include/worship_sse.php");
		source.onerror = function(e) {
		  return;
		};
    source.addEventListener('playlist', function(e) {
      if (document.getElementById('songlist').innerHTML != e.data) {
        document.getElementById('songlist').innerHTML = e.data;
      }
    }, false);

    source.addEventListener('broadcast', function(e) {
      var obj = JSON.parse(e.data);
			if(WorshipStatus != obj.status) {
				WorshipStatus = obj.status;
			}
      if(obj.status == "ON-AIR") {
        if(parseInt(getCookie("<?php echo CK_LIVE_SONG; ?>")) === -1 || "<?php echo $validUser?>" !== obj.username) {
					if(parseInt(getCookie("<?php echo CK_LIVE_SONG; ?>")) != obj.song) {
            setCookie("<?php echo CK_LIVE_SONG; ?>", obj.song, 1);
          }
          if(parseInt(getCookie("<?php echo CK_LIVE_SEQ; ?>")) !== obj.sequence) {
            setCookie("<?php echo CK_LIVE_SEQ; ?>", obj.sequence, 1);
          }
        }
        if(document.getElementById('followmode').checked) {
					HostRefresh = true;
          if(parseInt(getCookie("<?php echo CK_LIVE_SONG; ?>")) != song.id) {
            retrieveSong(parseInt(getCookie("<?php echo CK_LIVE_SONG; ?>")));
          }
          if(sheet.currentSequence != obj.sequence) {
            sheet.currentSequence = obj.sequence;
            slideControl(0);
          }
					HostRefresh = false;
        }
      }
<?php
if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_BROADCAST,$validRoles)) {
echo <<<HEREDOC
      if("$validUser" != 'Login') {
        if(obj.status == "OFF-AIR") {
          if(document.getElementById('worshipStatus').innerHTML.includes(obj.status) == false) {
            document.getElementById('broadcastControls').innerHTML = '<button class="w3-btn w3-white w3-border w3-border-blue w3-hover-blue w3-round" onclick="broadcastControls(\'start\');"><i class="fas fa-play"></i> Start Broadcast</button>';
            if(document.getElementById('followmodediv').style.display == 'none') {
              document.getElementById('followmode').checked = true;
              document.getElementById('followmodediv').style.display = '';
              setCookie("{$const['CK_LIVE_SONG']}", -1, 1);
              setCookie("{$const['CK_LIVE_SEQ']}", -1, 1);
            }
						slideControl(0);
          }
        } else if(obj.status == "ON-AIR") {
          if(document.getElementById('worshipStatus').innerHTML.includes(obj.status) == false || document.getElementById('worshipStatus').innerHTML.includes("<b><u>" + obj.username + "</u></b>") == false) {
            if("$validUser" == obj.username) {
              document.getElementById('broadcastControls').innerHTML = '<button class="w3-btn w3-white w3-border w3-border-red w3-hover-red w3-round" onclick="broadcastControls(\'end\');"><i class="fas fa-stop"></i> End Broadcast</button>';
              document.getElementById('followmodediv').style.display = 'none';
              document.getElementById('followmode').checked = false;
							slideControl(0);
            } else {
              document.getElementById('broadcastControls').innerHTML = '<button class="w3-btn w3-white w3-border w3-border-red w3-hover-red w3-round" onclick="broadcastControls(\'override\');"><i class="fas fa-hand-rock"></i> Override Broadcast</button>';
              if(document.getElementById('followmodediv').style.display == 'none') {
                document.getElementById('followmode').checked = true;
                document.getElementById('followmodediv').style.display = '';
              }
							slideControl(0);
            }
          }
        }
      }
HEREDOC;
}
?>
    }, false);

    source.addEventListener('worshipStatus', function(e) {
      if (document.getElementById('worshipStatus').innerHTML != e.data) {
        document.getElementById('worshipStatus').innerHTML = e.data;
      }
    }, false);
} else {
    document.getElementById("songlist").innerHTML = "Sorry, your browser does not support server-sent events...";
}
</script>
<!-- Scripts END -->

</head>
<body class="w3-animate-opacity">
<?php require_once 'include/header.php'; ?>
<div class="w3-cell-row">
  <!-- Song List -->
	<div class="w3-sidebar w3-collapse w3-animate-left w3-col l4 m6" style="z-index:3;position:absolute;overflow-y:hidden;" id="slSidebar">
    <div class="w3-container w3-blue w3-card">
      <h2 class="w3-wide w3-center"><label>Song List</label></h2>
    </div>
		<a class="w3-button w3-block w3-card w3-hide-large w3-large w3-red" href="javascript:void(0)" onclick="close_sl()"><i class="fas fa-times"> Close</i></a>
    <div class="w3-container w3-section">
      <ul class="w3-ul w3-hoverable w3-container" id="songlist" onclick="selectSong(event)" style="cursor:pointer;overflow-y:auto;width:100%;">
      </ul>
    </div>
  </div>
	<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="close_sl()" style="cursor:pointer" id="slOverlay"></div>
  <div class="w3-main w3-animate-left" style="margin-left:33%;" id="content">
    <div class="w3-container w3-blue w3-card">
      <h2 class="w3-wide w3-center">
				<button type="button" class="w3-button w3-hide-large w3-col m6 w3-blue-gray w3-card" style="padding:0px; margin-bottom:10px;" onclick="open_sl();"><i class="fas fa-clipboard-list"></i> Songs</button>
			  <label class="w3-col m6 l12" style="padding:0px; margin-bottom:10px;">Music Sheet</label>
	    </h2>
    </div>
	  <!-- Music Sheet -->
  	<div id="musicsheettab">
      <div class="w3-container">
        <br>
      	<div class="w3-row-padding">
          <!-- Worship Status (ON-AIR / OFF-AIR) START -->
      		<div class="w3-col l4 m4 s6 w3-panel" id="worshipStatus">
          </div>
          <!-- Worship Status (ON-AIR / OFF-AIR) END -->

          <!-- Follow Broadcaster START -->
      		<div class="w3-col l4 m4 s6 w3-panel">
            <div id="followmodediv">
        			<input type="checkbox" class="w3-check" id="followmode" onclick="slideControl(0)" checked>
        			<label class="w3-text-dark-gray" style="font-size:16px; font-weight:bold;" for="followmode">Follow Broadcaster</label>
            </div>
          </div>
          <!-- Follow Broadcaster END -->
<?php
if(array_intersect($RL_SONGSFULL,$validRoles) || array_intersect($RL_BROADCAST,$validRoles)) {
echo <<<'HEREDOC'
          <!-- Broadcasting Control START -->
          <div class="w3-col l4 m4 w3-panel" id="broadcastControls">
          </div>
          <!-- Broadcasting Control END -->
HEREDOC;
}
?>
      	</div>
        <?php require_once 'include/song_sheet.php'; ?>
      </div>
  	</div>
  </div>
</div>
<?php require_once 'include/footer.php'; ?>
</body>
</html>
